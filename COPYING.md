# COPYING #
The source code in portfolio.connorford.cocotab has been provided for the sole purpose of inspection
by Apadmi Ltd.

By cloning this repository you are agreeing that you will not reproduce, distribute or create
derivative works from the code provided in that package.