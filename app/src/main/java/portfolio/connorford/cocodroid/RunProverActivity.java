package portfolio.connorford.cocodroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import portfolio.connorford.cocotab.tableau.DeductionSet;
import portfolio.connorford.cocotab.tableau.Tableau;
import portfolio.connorford.cocotab.tableau.WorldRelation;
import portfolio.connorford.cocotab.logic.TableauFormula;
import portfolio.connorford.cocotab.logic.World;

public class RunProverActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run_prover);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //default values
        PreferenceManager.setDefaultValues(this,R.xml.pref_frame_conditions, false);
        //read prover preference values
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((TextView)findViewById(R.id.textView)).setText("");
                String conditions[] = {"reflexive", "symmetric", "transitive", "serial"};

                ArrayList<String> frameConditions = new ArrayList<String>();
                //determine frame conditions from preferences
                for(String conditionPreference : conditions) {
                    if (sharedPref.getBoolean(conditionPreference, false))
                        frameConditions.add(conditionPreference);
                }



                ArrayList<World> worlds = new ArrayList<World>();
                Tableau tableau = new Tableau();

                //tableau preferences
                Tableau.blocking = sharedPref.getBoolean("pref_blocking", false);
                Tableau.showBranch = sharedPref.getBoolean("pref_branch", false);
                Tableau.showModel = sharedPref.getBoolean("pref_model", false);
                if(sharedPref.getBoolean("pref_alt_rule_order", false))
                    Tableau.setRuleOrder(1);

                Boolean negateFormula = sharedPref.getBoolean("pref_negate", false);

                //get formula
                EditText text = findViewById(R.id.editText);

                try {
                    TableauFormula fml = new TableauFormula("a",text.getText().toString());
                    if(negateFormula)
                        fml = fml.getNegation();

                    //run tableau derivation
                    tableau.derivation(null,
                            frameConditions,
                            new DeductionSet<World>(),
                            new DeductionSet<WorldRelation>(),
                            fml);

                    String result = "The problem was" + (tableau.isSatisfiable() ? " " : " not ") + "satisfiable";
                    Snackbar.make(view, result, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    if(tableau.isSatisfiable())
                        ((TextView)findViewById(R.id.textView)).setText(tableau.currBranch.toString());

                } catch (Exception e) {
                    System.err.print(e);
                    Snackbar.make(view, "error during parsing or derivation", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_run_prover, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
