package portfolio.connorford.cocotab.tableau;
import portfolio.connorford.cocotab.logic.World;

/**
 * Represent world relations.
 * resulting from tableau rules.
 */
public class WorldRelation extends WorldAssociation {

  /**
   * Class constructor.
   * for a fresh relation
   */
  public WorldRelation(World w1, World w2) {
    super(w1, w2);
  }

  public String toString() {
    return "R(" + first + ", " + second + ")";
  }
  
  public boolean equals(Object o) {
    if (!(o instanceof WorldRelation))
      return false;
    if (o == this)
        return true;
  
    WorldRelation r = (WorldRelation) o;
    return r.hashCode() == hashCode();
  }

  //2 methods for deep copy for branch splitting
  private WorldRelation(WorldRelation wr) {
    super(wr.first.copyOf(), wr.second.copyOf());
  }

  public WorldRelation copyOf() {
    return new WorldRelation(this);
  }
}
