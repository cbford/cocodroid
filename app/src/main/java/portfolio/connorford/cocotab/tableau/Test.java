package portfolio.connorford.cocotab.tableau;

import portfolio.connorford.cocotab.congruenceClosure.CRule;
import portfolio.connorford.cocotab.congruenceClosure.CongruenceClosure;
import portfolio.connorford.cocotab.congruenceClosure.KTerm;
import portfolio.connorford.cocotab.logic.World;
import portfolio.connorford.cocotab.congruenceClosure.DRule;
import portfolio.connorford.cocotab.congruenceClosure.WitnessWorld;

/**
 * Class for testing congruence closure and tableau derivation.
 * Used for development only.
 */
public class Test
{
  private static Branch currBranch;
            
  public static void main(String args[])
  {
    Branch.printDerivation = true;

    System.out.println("paper problem");
    paperProbelm();

    System.out.println("Parametric problem 1");
    parametricTest1(5);

    System.out.println("Parametric problem 2");
    parametricTest2(5);

    System.out.println("Parametric problem 3");
    parametricTest3(5);

    testDeductionProblem();
  }

  private static void testDeductionProblem()
  {
    World a = new World("a");

    KTerm c0 = new KTerm(),
          c1 = new KTerm(),
          c2 = new KTerm(),
          c3 = new KTerm();

    WitnessWorld w1 = new WitnessWorld("f-phi1", c0),
                 w2 = new WitnessWorld("f-phi2", c0),
                 w3 = new WitnessWorld("f-phi2", c0);

    DeductionSet<World> worlds = new DeductionSet<World>();
    worlds.add(a);
    worlds.add(c0);
    worlds.add(c1);
    worlds.add(c2);
    worlds.add(c3);
    worlds.add(w1);
    worlds.add(w2);
    worlds.add(w3);

    currBranch = new Branch();
    KTerm.resetTermCount();
    Branch.resetBranchCount();

    currBranch.addDeduction(new DRule(a, c0), "given");
    currBranch.addDeduction(new DRule(w1, c1), "given");
    currBranch.addDeduction(new DRule(w2, c2), "given");
    currBranch.addDeduction(new DRule(w3, c3), "given");
    currBranch.addDeduction(new CRule(c1, c0), "given");
    currBranch.addDeduction(new CRule(c2, c0), "given");

    currBranch.addDeduction(new WorldEquality(c3, c0, true), "given");

    new CongruenceClosure(currBranch).applyShostak();
    System.out.print("result:\n\t" + currBranch.getRewriteRules() + '\t' + currBranch.getWorldEqualities());
  }

  private static void paperProbelm()
  {
    World a = new World("a"),
          b = new World("b"),
          ffa = genNestedWitness(a, 2),
          fb = genNestedWitness(b, 1);
  
    DeductionSet<World> worlds = new DeductionSet<World>();
    worlds.add(a);
    worlds.add(b);
    worlds.add(ffa);
    worlds.add(fb);

    //set up deduction system
    currBranch = new Branch();
    currBranch.addDeduction(new WorldEquality(a, b, true), "given");
    currBranch.addDeduction(new WorldEquality(ffa, fb, true), "given");

    //perform congruence closure rules
    new CongruenceClosure(currBranch).applyShostak();

    System.out.print("result:\n" + currBranch.getRewriteRules() + currBranch.getWorldEqualities());

  }

  private static void parametricTest1(int bound)
  {
    World nWit, mWit, iWit, jWit;

    //loop over parameters
    for(int i=1; i<bound; i++)
      for(int j=1; j<bound; j++)
        for(int m=1; m<bound; m++)
          for(int n=1; n<bound; n++)
          {
            if(n<=m || i<=j)
              continue;

            KTerm.resetTermCount();
            Branch.resetBranchCount();
            
            //generate witWorlds
            World a = new World("a");

            nWit = genNestedWitness(a, n); 
            mWit = genNestedWitness(a, m); 
            iWit = genNestedWitness(a, i); 
            jWit = genNestedWitness(a, j); 

            DeductionSet<World> worlds = new DeductionSet<World>();
            worlds.add(nWit);
            worlds.add(mWit);
            worlds.add(iWit);
            worlds.add(jWit);

            //determine if this is an expected case
            boolean shouldHold;
            System.out.println("-------");

            if((((i-j)%(n-m)) == 0) && j>=m)
            {
              System.out.printf("m=%d n=%d i=%d j=%d\n", m, n ,i, j);
              System.out.println("(n-m) | (i-j) and j>=m satisfied; should fail.");
              shouldHold = true;
            }
            else
            {
              System.out.println("(n-m) | (i-j) or j>=m not satisfied; may not fail.");
              shouldHold = false;
              continue;
            }

            //set up deduction system
            currBranch = new Branch();
            currBranch.addDeduction(new WorldEquality(nWit, mWit, true), "given");
            currBranch.addDeduction(new WorldEquality(iWit, jWit, false), "given");


            //perform congruence closure rules
            //while(congruenceClosure())
            //{}
            new CongruenceClosure(currBranch).applyShostak();

            System.out.print("result:\n" + currBranch.getRewriteRules() + currBranch.getWorldEqualities());

            if(shouldHold && !currBranch.isClosed())
            {
              System.out.println("Test failed.");
            }

            World nNormal = currBranch.getENormal(nWit),
                  mNormal = currBranch.getENormal(mWit),
                  iNormal = currBranch.getENormal(iWit),
                  jNormal = currBranch.getENormal(jWit);



            //System.out.printf("%s is %s = %s is %s: %s\n", nWit, nNormal, mNormal, mWit, nNormal.equals(mNormal));
            //System.out.printf("%s is %s = %s is %s: %s\n", iWit, iNormal, jNormal, jWit, iNormal.equals(jNormal));

          }
  }
  public static int gcd(int a, int b) {
    if (b==0)
      return a;
    
    return gcd(b,a%b);
  }

  private static void parametricTest2(int bound)
  {
    World nWit, mWit, kWit;

    //loop over parameters
    for(int k=1; k<bound; k++)
      for(int m=1; m<bound; m++)
        for(int n=1; n<bound; n++)
        {
          boolean shouldHold = ((k%gcd(m,n)) == 0);
          System.out.println("-------");

          if(shouldHold)
          {
            System.out.printf("gcd(%d,%d) = %d | %d\n", m ,n, gcd(m,n), k);
          }
          else
          {
            System.out.println("division condition fails");
            continue;
          }

          KTerm.resetTermCount();
          Branch.resetBranchCount();

          //generate witWorlds
          World a = new World("a");

          nWit = genNestedWitness(a, n); 
          mWit = genNestedWitness(a, m); 
          kWit = genNestedWitness(a, k); 

          DeductionSet<World> worlds = new DeductionSet<World>();
          worlds.add(nWit);
          worlds.add(mWit);
          worlds.add(kWit);

          //set up deduction system
          currBranch = new Branch();
          currBranch.addDeduction(new WorldEquality(nWit, a, true), "given");
          currBranch.addDeduction(new WorldEquality(mWit, a, true), "given");
          currBranch.addDeduction(new WorldEquality(kWit, a, false), "given");


          
          //perform congruence closure rules
          //while(!currBranch.isClosed() && shostack())
          //{}
          //while(congruenceClosure())
          //{}

          new CongruenceClosure(currBranch).applyShostak();
          System.out.print("result:\n\t" + currBranch.getRewriteRules() + currBranch.getWorldEqualities());

            if(shouldHold && !currBranch.isClosed())
            {
              System.out.println("Test failed.");
            }

          World nNormal = currBranch.getENormal(nWit),
                mNormal = currBranch.getENormal(mWit),
                kNormal = currBranch.getENormal(kWit),
                aNormal = currBranch.getENormal(a);



          //System.out.printf("%s is %s = %s is %s: %s\n", kWit, kNormal, aNormal, a, nNormal.equals(aNormal));

        }
  }

  private static void parametricTest3(int bound)
  {
    World a = new World("a"),
          b = new World("b"),
          nWit, n1Wit, n2Wit, n3Wit, mWit, kWit;

    int n, n1, n2, n3, m, k;

    for(n1=1; n1<=bound; n1++)
      for(n2=1; n2<=bound; n2++)
        for(n3=1; n3<=bound; n3++)
          for(m=1; m<=bound; m++)
            for(k=1; k<=bound; k++)
            {
              //check condition satisfied
              n = n3 - n2 + n1;
              boolean shouldHold = (k%gcd(m,n)==0) && n3>n2;

              if(!shouldHold)
                continue;

              System.out.printf("n1=%d n2=%d n3=%d n=%d m=%d k=%d\n", n1, n2, n3, n, m, k);

              KTerm.resetTermCount();
              Branch.resetBranchCount();

              n1Wit = genNestedWitness(a, n1);
              n2Wit = genNestedWitness(b, n2);
              n3Wit = genNestedWitness(b, n3);
              mWit = genNestedWitness(a, m);
              kWit = genNestedWitness(a, k);

              DeductionSet<World> worlds = new DeductionSet<World>();
              worlds.add(n1Wit);
              worlds.add(n2Wit);
              worlds.add(n3Wit);
              worlds.add(mWit);
              worlds.add(kWit);

              //set up deduction system
              currBranch = new Branch();
              currBranch.addDeduction(new WorldEquality(n1Wit, n2Wit, true), "given");
              currBranch.addDeduction(new WorldEquality(n3Wit, a, true), "given");
              currBranch.addDeduction(new WorldEquality(mWit, a, true), "given");
              currBranch.addDeduction(new WorldEquality(kWit, a, false), "given");

              new CongruenceClosure(currBranch).applyShostak();
              System.out.print("result:\n\t" + currBranch.getRewriteRules() + currBranch.getWorldEqualities());

              if(shouldHold && !currBranch.isClosed())
              {
                System.out.println("Test failed.");
              }
            }
    
  }

  private static World genNestedWitness(World a, int nest)
  {
    World ret = a;

    for(int i=0; i<nest; i++)
      ret = new WitnessWorld("f", ret);

    return ret;
  }
}
