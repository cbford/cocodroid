package portfolio.connorford.cocotab.tableau;

import portfolio.connorford.cocotab.congruenceClosure.KTerm;
import portfolio.connorford.cocotab.logic.World;

/**
 * Represents reasoning of equality of two terms
 */
public class WorldEquality extends WorldAssociation {
  //we can reason terms as equal or not equal
  public boolean equality;

  /**
   * Class constructor.
   * for a fresh equality
   */
  public WorldEquality(World w1, World w2, boolean b) {
    super(w1, w2);
    equality = b;
  }
  
  /**
   * @return true iff the equality states the worlds are equal
   */
  public boolean getEquality() {
    return equality;
  }

  /**
   * @return true iff either term is not a single KTerm(fresh constant)
   */
  public boolean canSimplify()
  {
    return !(first instanceof KTerm && second instanceof KTerm);
  }

  @Override
  //chars may not be supported
  public String toString() {
    return first
           + (equality ? "≈" : "≉")
           + second;
  }
  
  public boolean equals(Object o) {
    if (!(o instanceof WorldEquality))
      return false;
    if (o == this)
        return true;
  
    WorldEquality r = (WorldEquality) o;
    return ((first.equals(r.first) && second.equals(r.second)) ||
            (first.equals(r.second) && second.equals(r.first)))
           && (r.equality == equality);
  }

  //two methods deep copy, for branch splitting
  private WorldEquality(WorldEquality we) {
    super(we.first.copyOf(), we.second.copyOf());
    this.equality = we.equality;
  }

  public WorldEquality copyOf() {
    return new WorldEquality(this);
  }

}
