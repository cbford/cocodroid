package portfolio.connorford.cocotab.tableau;

import java.util.ArrayDeque;

import portfolio.connorford.cocotab.logic.TableauFormula;
import portfolio.connorford.cocotab.logic.World;
import portfolio.connorford.cocotab.congruenceClosure.CRule;
import portfolio.connorford.cocotab.congruenceClosure.DRule;
import portfolio.connorford.cocotab.congruenceClosure.KTerm;
import portfolio.connorford.cocotab.congruenceClosure.RewriteAction;
import portfolio.connorford.cocotab.congruenceClosure.RewriteRule;
import portfolio.connorford.cocotab.congruenceClosure.WitnessWorld;
import portfolio.connorford.cocotab.congruenceClosure.WorldPartitioning;
import portfolio.connorford.cocotab.logic.ModalFormula;
import portfolio.connorford.cocotab.logic.Operation;

/**
 * Represents a single branch of deduction in a Tableau proof
 */
public class Branch {

  //datastructures for various deductions on the branch
  private ArrayDeque<DeductionSet<?>> deductions; //all deductions
  private DeductionSet<TableauFormula> formulae, //top operator not modal
                                       boxFormulae, //top operator box
                                       diamondFormulae, //top operator diamond
                                       deducedFromBox, //result of applying box
                                       interpretation; //atoms in result
  private DeductionSet<World> worlds;
  private DeductionSet<WorldRelation> worldRelations;
  private DeductionSet<WorldEquality> worldEqualities;
  private WorldPartitioning worldPartitioning;
  private DeductionSet<DRule> dRules;
  private DeductionSet<CRule> cRules;
  //atoms not in intetpretation
  private DeductionSet<ModalFormula> atomsLeftToInterpret;
  //output format toggle
  public static boolean printDerivation;
  
  private boolean closed;
  public int derivationCount = 0,
              deductionCount = 0,
              prevDeductionCount = 0,
              branchID;
  private static int branchCount = 0,
                     deductionOutputWidth = 70;
  
  /**
   * Class constructor.
   * Instantiate all datastructures, reset class counts
   */
  public Branch() {
    formulae = new DeductionSet<TableauFormula>();
    boxFormulae = new DeductionSet<TableauFormula>();
    diamondFormulae = new DeductionSet<TableauFormula>();
    deducedFromBox = new DeductionSet<TableauFormula>();
    interpretation = new DeductionSet<TableauFormula>();
    worlds = new DeductionSet<World>();
    worldRelations = new DeductionSet<WorldRelation>();
    worldEqualities = new DeductionSet<WorldEquality>();
    dRules = new DeductionSet<DRule>();
    cRules = new DeductionSet<CRule>();
    worldPartitioning = new WorldPartitioning();
    atomsLeftToInterpret = new DeductionSet<ModalFormula>();
    
    deductions = new ArrayDeque<DeductionSet<?>>();
    deductions.add(formulae);
    deductions.add(boxFormulae);
    deductions.add(diamondFormulae);
    deductions.add(deducedFromBox);
    deductions.add(worlds);
    deductions.add(worldRelations);
    deductions.add(worldEqualities);
    deductions.add(dRules);
    deductions.add(cRules);

    closed = false;
    derivationCount = 0;
    deductionCount = 0;
    prevDeductionCount = 0;
    branchCount++;
    branchID = branchCount;
  }
  
  /**
   * Class constructor for taking deep copy.
   * used when splitting a branch.
   */
  public Branch(Branch b) {
    //take deep copy of all internal datastructures
    this.formulae = b.formulae.copyOf();
    this.interpretation = b.interpretation.copyOf();
    this.boxFormulae = b.boxFormulae.copyOf();
    this.diamondFormulae = b.diamondFormulae.copyOf();
    this.deducedFromBox = b.deducedFromBox.copyOf();
    this.worlds = b.worlds.copyOf();
    this.worldRelations = b.worldRelations.copyOf();
    this.worldEqualities = b.worldEqualities.copyOf();
    this.dRules = b.dRules.copyOf();
    this.cRules = b.cRules.copyOf();

    deductions = new ArrayDeque<DeductionSet<?>>();
    deductions.add(formulae);
    deductions.add(boxFormulae);
    deductions.add(diamondFormulae);
    deductions.add(deducedFromBox);
    deductions.add(worlds);
    deductions.add(worldRelations);
    deductions.add(worldEqualities);
    deductions.add(dRules);
    deductions.add(cRules);

    this.worldPartitioning = b.worldPartitioning.copy();
    this.atomsLeftToInterpret = b.atomsLeftToInterpret.copyOf();

    //reset relevant branch counts
    this.derivationCount = 0;
    this.deductionCount = 0;
    this.prevDeductionCount = 0;
    
    //identify the branch
    branchCount++;
    this.branchID = branchCount;
  }
  
  /**
   * @return true iff the branch has been closed by a closing tableau rule
   */
  public boolean isClosed() {
    return closed;
  }

  /**
   * @param atoms  atoms for which this branch should find an asignment
   */
  public void addAtomsToInterpret(DeductionSet<ModalFormula> atoms) {
    //don't readd atoms which we already interpret
    for(ModalFormula at : atoms){
      atomsLeftToInterpret.offer(at);
      for(TableauFormula tf : interpretation){
        atomsLeftToInterpret.remove(tf.getModalFormula());
        atomsLeftToInterpret.remove(tf.getModalFormula()
                                       .getComplement());
      }
    }
  }

  /**
   * @param atoms  atoms for which this branch need not find an asignment
   */
  public void removeAtomsToInterpret(DeductionSet<ModalFormula> atoms) {
    for(ModalFormula at : atoms){
      atomsLeftToInterpret.remove(at);
    }
  }

  /**
   * @return true iff all diamond and box formualae are satisfied
   */
  public boolean interpretsAllModalFormulae() {
   //System.out.println("checking modal formulae are respected");
   //System.out.println("diaFormulae: " + diamondFormulae);
    if(!diamondFormulae.isEmpty())
      return false;
    //return true;

   //System.out.println("boxFormulae: " + boxFormulae);
   for(TableauFormula box : boxFormulae){
     for(WorldRelation rel : getRelatedWorlds(box.getWorld())){
       //System.out.println(new TableauFormula(rel.getSecond(), box.getModalFormula().getSubformula()));
       //System.out.println(deducedFromBox);
       //System.out.println("is " + box + " respected in " + rel.getSecond());
       if(!deducedFromBox.contains(new TableauFormula(rel.getSecond(), box.getModalFormula().getSubformula())))
         return false;
       //if(!box.isRespectedIn(rel.getSecond()))
     }
   }
   return true;
  }

  /**
   * @return true iff branch closed or all formulae have been deduced upon
   */
  public boolean isExhausted() {
    //System.out.println("closed: " + closed);
    boolean isExhausted =  isClosed() || 
      (formulae.isEmpty() && interpretsAllModalFormulae());
    //System.out.println("checking exhaustion: " + isExhausted);
    //System.out.println("formulae " + formulae);
    //System.out.println("all atoms " + interpretsAllAtoms());
    //System.out.println("modals " + interpretsAllModalFormulae());
    return isExhausted;
  }

  /**
   * @return the remaining negated or disjunctive branch formulae on the branch
   */
  public DeductionSet<TableauFormula> getFormulae() {
    return formulae;
  }
  
  /**
   * @return the box formulae on the branch
   */
  public DeductionSet<TableauFormula> getBoxFormulae() {
    return boxFormulae;
  }
  
  /**
   * @return the remaining diamond formulae on the branch
   */
  public DeductionSet<TableauFormula> getDiamondFormulae() {
    return diamondFormulae;
  }
  
  /**
   * @return the worlds on the branch
   */
  public DeductionSet<World> getWorlds() {
    return worlds;
  }

  /**
   * @return the forest of worlds in the branch
   */
  public WorldPartitioning getWorldPartitioning()
  {
    return worldPartitioning;
  }
  
  /**
   * Find the smallest world under rewrites from the given world.
   *
   * @param w  the world to find the E Normal form of
   * @return the rewritten version of the world
   */
  public World getENormal(World w) {
    //System.out.println("get partition for " + w + ": "
    //                   + worldPartitioning.getPartition(w));
    return worldPartitioning.getInENormalForm(w);
  }
  
  /**
   * @return true iff the enormal of the given world is iteslf,
   *         so it cannot be rewritten as a smaller term.
   */
  public boolean isENormal(World w) {
    return w.equals(getENormal(w));
  }
  
  /**
   * @return asignment for atomic formulae on this branch
   */
  public DeductionSet<TableauFormula> getInterpretation() {
    return interpretation;
  }
  
  /**
   * @return collection of all worlds relations deduced on this branch
   */
  public DeductionSet<WorldRelation> getWorldRelations() {
    return worldRelations;
  }

  /**
   * @return collection of all worlds equalities deduced on this branch,
   *         that havent been converted to rewrites by congruence clousre rules
   */
  public DeductionSet<WorldEquality> getWorldEqualities() {
    return worldEqualities;
  }

  /**
   * @return true equalities that may be considered by the orientation rule
   */
  public DeductionSet<WorldEquality> getPositiveWorldEqualities() {
    DeductionSet<WorldEquality> ret = new DeductionSet<WorldEquality>();
    for(WorldEquality we : worldEqualities)
      if(we.getEquality())
        ret.add(we);

    return ret;
  }
  
  /**
   * @return all CRules and DRules deduced on this branch
   */
  public DeductionSet<RewriteRule> getRewriteRules() {
    DeductionSet<RewriteRule> rules = new DeductionSet<RewriteRule>();
    rules.addAll(cRules);
    rules.addAll(dRules);
    return rules;
  }
  
  /**
   * @return CRules deduced on this branch
   */
  public DeductionSet<CRule> getCRules() {
    return cRules;
  }
  
  /**
   * @return DRules deduced on this branch
   */
  public DeductionSet<DRule> getDRules() {
    return dRules;
  }
  
  /**
   * Find all b where the branch contains a relation R(w,b)
   *
   * @param w  the world in the first argument of the relation
   * @return collection of all such related worlds b
   */
  public DeductionSet<WorldRelation> getRelatedWorlds(World w) {
    DeductionSet<WorldRelation> relations = new DeductionSet<WorldRelation>();

    for(WorldRelation r : worldRelations){
      if(r.getFirst().equals(w))
        relations.offer(r);
    }
    return relations;
  }
  
  /**
   * Format branch as string for output.
   * Show all relevant information about the deriavations
   * and if the branch is closed or open.
   */
  public String toString() {
    String ret = "\nBranch " + branchID + " - "
                 + (isClosed()? "closed\n" : "open\n");

    ret += "Interpretation:\n" + (isClosed() ? "⊥\n" : interpretation);
    ret += "Worlds:\n" + worlds;
    ret += "Relations:\n" + worldRelations;
    ret += "Equalities:\n" + worldEqualities;
    ret += "Rewrites:\n" + cRules + dRules;
    ret += "Formulae:\n" + formulae;
    ret += "Box formulae:\n" + boxFormulae;
    
    return ret + '\n';
  }

  public String getModel() {
    if(isClosed())
      return "model not found";
    else if(!isExhausted())
      return "branch not yet exhausted";
    
    String ret = "";
    ret += "Interpretation:\n" + (isClosed() ? "⊥\n" : interpretation);
    ret += "Worlds:\n" + worlds;
    ret += "Relations:\n" + worldRelations;
    return ret;
  }

  /**
   * @return the integer uniquely identifying the branch
   */
  public int getBranchID() {
    return branchID;
  }
  
  /**
   * Produce a formatted string to be added to the output derivation.
   *
   * @param ded  A deduction on the branch - may be a string about deletion
   * @param reason  Why the deduction has been added to the branch
   * @return a formatted output line for a deduction added to the branch
   */
  private String stringOfDeduction(Object ded, String reason) {
    String indexCol = "",
           gutter1 = "",
           dedCol = ded.toString(),
           gutter2 = "",
           reasonCol = (reason.equals("given")) ? reason : "by " + reason;

    //determine the index to print out
    indexCol = branchID + "." + (++derivationCount);
    //index col is fixed width of 6
    if(indexCol.length() < 6)
      indexCol += new String(new char[6 - indexCol.length()])
                               .replace("\0", " ");
    //determine the first gutter width - depends on the branch
    for(int i=0; i<branchID*2; i++)
      gutter1 += "  ";
    //determine the second gutter width - depends on remaining space in line
    int spareSpace = deductionOutputWidth
                     - (indexCol.length()
                        + gutter1.length()
                        + dedCol.length()
                        + reasonCol.length());
    if(spareSpace > 0)
      gutter2 += new String(new char[spareSpace]).replace("\0", " ");
    else {
      gutter2 = "\n"; //overflow into new line if output is too long for one
      gutter2 += new String(new char[deductionOutputWidth-reasonCol.length()])
                              .replace("\0", " ");
    }
    
    return indexCol + gutter1 + dedCol + gutter2 + reasonCol;
  }

  /**
   * Add a deduction resulting from a tableau rule to the branch.
   * A deduction is a formulae, relation, equality, rewrite rule or rewrite.
   * Offer methods are used to prevent adding duplicate deductions.
   *
   * @param o  The deduction being added.
   * @param reason  What rule has been applied to add the deduction
   * @return true iff new information has been added to the branch
   */
  public boolean addDeduction(Object o, String reason) {
    //System.out.println("called addDeduction for " + o + ", " + reason);

    String deductionLine;

    if(isClosed())  return false;
    try{
      if(o instanceof TableauFormula) {
        TableauFormula f = (TableauFormula) o;
        //fix double bracketing of single disjunct formulae
        if(f.getModalFormula().getOperator() == Operation.OR 
           && f.getSubformulae().size() == 1) {
            //System.out.println("removing outer brackets of " + f);
            f = new TableauFormula(f.getWorld(), f.getSubformulae().get(0));
            //this line for deduction in printDeduction
            o = f;
           }
        boolean newF = false;

       //add the world to the branch if we dont already know about it
       worlds.offerFirst(f.getWorld());

       //atomic formuale can be top, bot or a variable(possibly negated)
       if(f.getModalFormula().isAtomic())
       {
         //bot closes a branch by the bot rule
         if(f.getModalFormula().equals(new ModalFormula("⊥")))
         {
           deductionLine = stringOfDeduction(f, reason);
           System.out.println(deductionLine);
           //apply bot rule
           addDeduction(new ModalFormula("⊥"), "(⊥)");
           return true;
         }
         //true holds in all worlds
         else if(f.getModalFormula().equals(new ModalFormula("T")))
         {
           //we may still print that the deduction has been made
           deductionLine = stringOfDeduction(f, reason);
           System.out.println(deductionLine);
           return false;
         }
         else
         {
           //only add new atoms. may add a contradiction that will later
           //be found by the cl rule
           
           //track box consequences
           if(reason.contains("(◻)") && !deducedFromBox.offer(f)){
               return false;
           }
           if(interpretation.offer(f))
           {
             deductionLine = stringOfDeduction(f, reason);
             System.out.println(deductionLine);
             //remove from the list of atoms we need to interpret
             atomsLeftToInterpret.remove(f.getModalFormula());
             atomsLeftToInterpret.remove(f.getModalFormula()
                                           .getComplement());
             return true;
           }
           //we are adding something already in the interpretation
           else
           {
            return false;
           }
         }
       }
       
          
       //add box related formulae to the right branch collection
       if(reason.contains("(◻)") && !deducedFromBox.offer(f)){
           return false;
       }
       else
       {
        //System.out.println("not boxy: " + f);
       }

       if(f.getModalFormula().getOperator() == Operation.BOX)
         newF = boxFormulae.offer(f);
       //add diamond formulae to the right branch collection
       else if(f.getModalFormula().getOperator() == Operation.NOT
               && f.getSubformula().getOperator() == Operation.BOX)
         newF = diamondFormulae.offerLast(f);
       //push alphas, enqueue others
       //This can be used to sort nonmodal formulae
       else if(f.getModalFormula().getOperator().equals(Operation.NOT)
               && f.getSubformula().getOperator().equals(Operation.OR))
         newF = formulae.offerFirst(f);
       else
         newF = formulae.offerLast(f);
       
       //exit without printing deduction if nothing new added
       if(!newF)
           return false;

      }
      else if(o instanceof ModalFormula) {
        ModalFormula f = (ModalFormula) o;
        
        //only bot can be pushed as a deduction
        if(f.getAtom().equals("⊥"))  //apply (⊥)
          closed = true;
        else
          throw new IllegalArgumentException("Tried to add modal formula "
                                             + f
                                             + " to branch (only ⊥ allowed)");
      }
      //handle an instruction to rewrite some deduction
      else if (o instanceof RewriteAction) {
        RewriteAction rw = (RewriteAction) o;
        if(rw.getDeductionToRewrite() instanceof WitnessWorld)
        {
          if(rw.getDeductionToRewrite().equals(rw.getRewriteFrom()))
          {
            deductionLine = stringOfDeduction(rw, reason);
            System.out.println(deductionLine);
            worlds.remove(rw.getRewriteFrom());
            worlds.offer(rw.getRewriteTo());
            return true;
          }
        }
        //check we aren't about to rewrite a contradiction
        if(!rw.doRewrite(reason))
          return false;
        else {
          boolean rewriteAddedNewDeduction = false;
          if(rw.getDeductionToRewrite() instanceof TableauFormula) {
            deductionLine = stringOfDeduction(rw, reason);
            System.out.println(deductionLine);
            rewriteAddedNewDeduction = addDeduction(rw.getDeductionToRewrite(),
                                                    reason);
          }
          else if(rw.getDeductionToRewrite() instanceof WorldAssociation){
            deductionLine = stringOfDeduction(rw, reason);
            System.out.println(deductionLine);
            rewriteAddedNewDeduction = addDeduction(rw.getDeductionToRewrite(),
                                                    reason);
            rewriteAddedNewDeduction = true;
          }
          else if(rw.getDeductionToRewrite() instanceof World){
            deductionLine = stringOfDeduction(rw, reason);
            System.out.println(deductionLine);
            rewriteAddedNewDeduction = true;
          }
          else if(rw.getDeductionToRewrite() instanceof DRule){
            //check we are doing a collapse
            if(!reason.equals("collapse"))
              throw new Exception("Tried to rewrite drule "
                                  + rw.getDeductionToRewrite() + ". reason : " + reason);

            deductionLine = stringOfDeduction(rw, reason);
            System.out.println(deductionLine);
            rewriteAddedNewDeduction = true;
          }
          worlds.remove(rw.getRewriteFrom());
          worlds.offer(rw.getRewriteTo());

          for(DeductionSet<?> ded : deductions)
            ded.removeDuplicates();

          interpretation.removeDuplicates();

          return rewriteAddedNewDeduction;
        }
      }
      else if (o instanceof WorldRelation) {
        WorldRelation r = (WorldRelation) o;
        //only add new relations
        if(!worldRelations.offer(r)) return false;
      }
      else if (o instanceof WorldEquality) {
        WorldEquality e = (WorldEquality) o;
        //apply (id)
        if(e.getFirst().equals(e.getSecond()) && !e.getEquality()) {
            deductionLine = stringOfDeduction(o, reason);
            System.out.println(deductionLine);
          addDeduction(new ModalFormula("⊥"), "(id)");
          return false;
        }
        if(reason.contains("deletion")) {
          //verify deletion is valid
          if((e.getFirst().equals(e.getSecond())) && e.getEquality())
          {
            worldEqualities.remove(e);
            deductionLine = stringOfDeduction("delete " + e, reason);
            System.out.println(deductionLine);
            return true;
          }
          else
          {
            throw new Exception("Tried to apply deletion on invalid equality: " + e);
          }
        }
        //only add new equalities
        if(!worldEqualities.offer(e)) return false;
        else {
            deductionLine = stringOfDeduction(o, reason);
            System.out.println(deductionLine);
          return false;
        }
      }
      else if (o instanceof CRule) {
        if(reason.equals("deduction"))
        {
          cRules.remove(o);
            deductionLine = stringOfDeduction("delete " + o, reason);
            System.out.println(deductionLine);
          return true;
        }
        CRule rule = (CRule) o;
        if(!cRules.offer(rule))  //only add new 
           return false;
        //add worlds to correct partitions
        worldPartitioning.addWorldsToPartition(rule.getFirst(),
                                                  rule.getSecond());
      }
      else if (o instanceof DRule) {
        if(reason.equals("deduction"))
        {
          dRules.remove(o);
            deductionLine = stringOfDeduction("delete " + o, reason);
            System.out.println(deductionLine);
          return true;
        }
        DRule rule = (DRule) o;
        //System.out.println("addDeduction called for DRule " + rule);
        worlds.offer(rule.getSecond());
        if(!dRules.offer(rule))  //only add new 
           return false;
        //add worlds to correct partitions
        worldPartitioning.addWorldsToPartition(rule.getFirst(),
                                                  rule.getSecond());
      }
      else {
        throw new IllegalArgumentException("Tried to add a deduction but"
                                           + "couldn't recognise object."
                                           + "\nReason: " + reason
                                           + "\nObject: " + o);
      }
      deductionCount++;
      
            deductionLine = stringOfDeduction(o, reason);
            System.out.println(deductionLine);
      return true;
    }
    catch(Exception e) {
      System.out.println(e);
    }
    return false;
  }
  
  /**
   * Look for a formula on this branch.
   *
   * @param f  the formula to try and find
   * @return true iff the given formula is in a datastructure on this branch
   */
  public boolean containsFormula(TableauFormula f) {
      return formulae.contains(f)
          || boxFormulae.contains(f)
          || diamondFormulae.contains(f)
          || deducedFromBox.contains(f)
          || interpretation.contains(f);
  }
  
  /**
   * Get terms on this branch with rewritable subterms
   */
  public DeductionSet<Deduction> getNTerms() {
    DeductionSet<Deduction> nTerms = new DeductionSet<Deduction>();
    nTerms.addAll(formulae);
    nTerms.addAll(boxFormulae);
    nTerms.addAll(diamondFormulae);
    nTerms.addAll(deducedFromBox);
    nTerms.addAll(interpretation);
    nTerms.addAll(worldRelations);
    nTerms.addAll(worldEqualities);
    nTerms.addAll(worlds);

    nTerms.removeDuplicates();

    
    return nTerms;
  }

  /**
   * Exhaustively apply extension and simplification on the branch.
  /* (ext.sim*)* is applied before tableau deduction, to replace all terms
   * with fresh constants so cc rules can be applied
   */
  public void congruenceClosurePreprocess() {
    if(printDerivation)
     System.out.println("Begin prepocessing.");
    DeductionSet<Deduction> possibleNTerms = getNTerms();

    //System.out.print("possivle n terms: " + possibleNTerms);
    
    boolean newRewrite;
    DRule rule;
    World writeFrom = null;
    KTerm writeTo = null;
    Deduction writeIn = null;
    
    while(!possibleNTerms.isEmpty()) {
      newRewrite = false;
      Deduction nTerm = possibleNTerms.pop();
      if(nTerm instanceof TableauFormula) {
        TableauFormula nFormula = (TableauFormula) nTerm;
        World from = nFormula.getWorld();
        if(!(from instanceof KTerm)) {
          //new rewrites can be done
          newRewrite = true;
          writeIn = nTerm;
          writeFrom = from;
          writeTo = new KTerm();
        }
      }
      else if(nTerm instanceof WorldRelation || nTerm instanceof WorldEquality) {
        WorldAssociation nWA = (WorldAssociation) nTerm;
        World first = nWA.getFirst(),
              second = nWA.getSecond();
        if(!(first instanceof KTerm)) {
          //new rewrites can be done
          newRewrite = true;
          writeIn = nWA;
          writeFrom = first;
          writeTo = new KTerm();
        }
        else if(!(second instanceof KTerm)) {
          //new rewrites can be done
          newRewrite = true;
          writeIn = nWA;
          writeFrom = second;
          writeTo = new KTerm();
        }
      }
      else if(nTerm instanceof World) {
        World nWorld = (World) nTerm;
        //we can rewrite worlds and subterms of witness worlds
        newRewrite = true;
        writeIn = nWorld;
        writeFrom = nWorld;
        writeTo = new KTerm();
      }

      if(newRewrite) {
        rule = new DRule(writeFrom, writeTo);
        //apply extension
        addDeduction(rule, "extension");
        RewriteAction rw = new RewriteAction(writeIn, writeFrom, writeTo);
        addDeduction(rw, "extension");
         //apply simplification
        for(Deduction d : possibleNTerms)
          addDeduction(new RewriteAction(d, writeFrom, writeTo), "simplification");
        possibleNTerms.remove(writeFrom);
        
      }
    }
    if(printDerivation)
      System.out.print("End prepocessing.\n");
  }

  //these two methods for class count variable
  public static void resetBranchCount() {
    branchCount = 0;
  }

  public static int getBranchCount() {
    return branchCount;
  }
}
