package portfolio.connorford.cocotab.tableau;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.logging.Logger;

import portfolio.connorford.cocotab.congruenceClosure.CRule;
import portfolio.connorford.cocotab.congruenceClosure.CongruenceClosure;
import portfolio.connorford.cocotab.congruenceClosure.DRule;
import portfolio.connorford.cocotab.congruenceClosure.KTerm;
import portfolio.connorford.cocotab.congruenceClosure.WitnessWorld;
import portfolio.connorford.cocotab.logic.ModalFormula;
import portfolio.connorford.cocotab.logic.Operation;
import portfolio.connorford.cocotab.logic.TableauFormula;
import portfolio.connorford.cocotab.logic.World;

/**
 *  Represents a semantic tableau.
 *  The deduction tableau, collection of branches 
 */
@SuppressWarnings("serial")
public class Tableau extends ArrayDeque<Branch>{
  //bounds on deduction
  private static int maxBranches = 1000,
                     maxWitnesses = 1000,
                     maxKTerms = 1000,
                     ruleOrder = 2;
  private int countUB, countCC, countBox, countNotBox;

  private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
  //collection of branches needing no further deduction
  private ArrayDeque<Branch> exhaustedBranches;
  //structures used when splitting occurs
  private ArrayDeque<Deduction> newBranchDeductions;
  private ArrayDeque<String> newBranchDeductionReasons;
  private ArrayList<String> frameConditions;
  //the branch where we make deductions
  public Branch currBranch;
  //do we show a model in output?
  public static boolean showModel = false, showBranch = false, blocking = true;
  private boolean satisfiable = false;
  
  /**
   * Class constructor.
   * Instantiate internal datastructures.
   */
  public Tableau() {
    exhaustedBranches = new ArrayDeque<Branch>();
    newBranchDeductions = new ArrayDeque<Deduction>();
    newBranchDeductionReasons = new ArrayDeque<String>();

    countUB = 0;
    countCC = 0;
    countBox = 0;
    countNotBox = 0 ;
  }

  public boolean isSatisfiable()
  {
    return satisfiable;
  }

  /**
   * Change rule order application.
   * See main loop  below for the two alternatives.
   */
  public static void setRuleOrder(int r) {
    ruleOrder = r;
  }

  /**
   * Check rule order application.
   */
  private int getRuleOrder() {
    return ruleOrder;
  }

  private void logStatus()
  {
    LOGGER.info(Branch.getBranchCount() + ","
              + currBranch.getBranchID() + ","
              + currBranch.getWorlds().size() + ","
              + currBranch.getInterpretation().size() + ","
              + countUB + ","  + countCC + ","
              + countBox + "," + countNotBox + ","
              + isSatisfiable());
  }

  /**
   *  Apply tableau rules exhaustively.
   *  Given some intial conditions for the tableau,
   *  deduce if a given formula is satisfiable.
   */
  public void derivation(String logFileName,
                         ArrayList<String> frConds,
                         DeductionSet<World> worlds,
                         DeductionSet<WorldRelation> worldRelations,
                         TableauFormula tf) {

    try {
      Profiler.setUp(logFileName);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException("problem creating log files.");
    }

    LOGGER.info("--------");

    //intial tableau conditions
    frameConditions = frConds;
    Branch b = new Branch();
    for(WorldRelation wr : worldRelations)
      b.addDeduction(wr, "given");
    b.addDeduction(tf, "given");

    //track what we are looking to interpret
    b.addAtomsToInterpret(tf.getModalFormula().getAtomsToInterpret());

    //preprocess the branch so we can apply congruence closure rules
    b.congruenceClosurePreprocess();

    //add the inital branch to the tableau
    push(b);
    
    //track if we have yet found a satisfying assignment
    boolean foundAnInterpretation = false;

    //ORDER BASED ON RULEORDER.TXT
    while((getRuleOrder() == 1) && !foundAnInterpretation && !isEmpty())
    {
      currBranch = getNextBranch();
      logStatus();

      //branch deduction loop
      while(!cl() && !currBranch.isExhausted())
      {
        logStatus();
        //closure rules
        if(dp2())
          continue;

        if(cl())
          break;

        //alpha exhaustively
        //alpha all sit at top of formula stack
        TableauFormula f;
        while(!currBranch.getFormulae().isEmpty()
              && ((f = currBranch.getFormulae().peek()).getModalFormula().getOperator().equals(Operation.NOT)
                  && f.getSubformula().getOperator().equals(Operation.OR)))
        {
          logStatus();
          currBranch.getFormulae().pop();
          alpha(f);
        }

        box();

        tr();
        
        logStatus();

        //attempt to find a beta to apply
        for(TableauFormula potentialBeta : currBranch.getFormulae())
        {
          if(potentialBeta.getModalFormula().getOperator().equals(Operation.OR))
          {
            beta(potentialBeta);
            currBranch.getFormulae().remove(potentialBeta);
            break;
          }
          else
          {
            //System.out.println("didn't act on " + potentialBeta);
            //System.out.println("unary? " + potentialBeta.getModalFormula().isAtomic());
          }
          logStatus();
        }//beta

        if(currBranch.isClosed())
          break;

        //blocking
        ub();
        logStatus();
        //while(congruenceClosure())
        //{}

				new CongruenceClosure(currBranch).applyShostak();
        countCC++;

        notBox();

        dp1();

        dp3();
        logStatus();
      }//branch deduction

      foundAnInterpretation = branchGivesInterpretation(currBranch);
    }//tab loop

    //Second rule order
    while((getRuleOrder() == 2) && !foundAnInterpretation && !isEmpty())
    {
      currBranch = getNextBranch();
      logStatus();

      //main rule application deduction loop
      //while(!currBranch.isExhausted())
      //while(!cl() && !currBranch.isExhausted())
      boolean stillDeducing = true, appliedRule = false;
      while(stillDeducing)
      {
      logStatus();
        //System.out.println("top of loop");
        appliedRule = false;
        appliedRule = appliedRule | box();
        //boolean rules loop
        boolean madeBooleanDeduction = false;
        do
        {
      logStatus();
          madeBooleanDeduction = false;
          //attempt to close
          if(cl() || dp2())
          {
            stillDeducing = false;
            continue;
          }

          DeductionSet<TableauFormula> alphaBetaFormulae = currBranch.getFormulae();
          boolean appliedBeta = false;
          while(!alphaBetaFormulae.isEmpty())
          {
      logStatus();
            appliedBeta = false;
            //take a formula
            TableauFormula f = alphaBetaFormulae.pop();

            //determine what rule to apply based on the formula operation
            if(f.getModalFormula().getOperator().equals(Operation.NOT)
               && f.getModalFormula().getSubformula().getOperator().equals(Operation.OR))
            {
              //alpha
              currBranch.getFormulae().remove(f);
              alpha(f);
              madeBooleanDeduction = true;
              appliedRule = true;
            }
            else if(f.getModalFormula().getOperator().equals(Operation.OR)
                    && !appliedBeta)
            {
              //beta - only once a round
              appliedBeta = true;
              currBranch.getFormulae().remove(f);
              beta(f);
              madeBooleanDeduction = true;
              appliedRule = true;
            }
            else
            {
              //System.out.println("didnt apply beta on " + f);
            }
          } // alpha beta loop
        } while(madeBooleanDeduction); // boolean rules loop

        appliedRule = appliedRule | refl();
        appliedRule = appliedRule | sym();

        appliedRule = appliedRule | ub();
        appliedRule = appliedRule | dp3();
        new CongruenceClosure(currBranch).applyShostak();
        countCC++;

        appliedRule = appliedRule | tr();
        appliedRule = appliedRule | notBox();
        appliedRule = appliedRule | dp1();

      logStatus();
        if(currBranch.isClosed() || !appliedRule)
          stillDeducing = false;
      }// branch deduction loop

      foundAnInterpretation = branchGivesInterpretation(currBranch);
    }// tableau loop

    if(foundAnInterpretation){
      System.out.println("satisfiable");
      satisfiable = true;
      if(showBranch)
        System.out.println(currBranch);
      else if(showModel)
        System.out.println(currBranch.getModel());
    }
    else
      System.out.println("unsatisfiable");

    logStatus();
  }

  //methods below correspond to tableau deduction rules

  /**
   * Close branches if we have contradictory formulae
   */
  public boolean cl() {
    DeductionSet<TableauFormula> possibleCLs = new DeductionSet<TableauFormula>();
    possibleCLs.addAll(currBranch.getFormulae());
    possibleCLs.addAll(currBranch.getBoxFormulae());
    possibleCLs.addAll(currBranch.getDiamondFormulae());
    possibleCLs.addAll(currBranch.getInterpretation());

    //look for a contradiction
    for(TableauFormula f : possibleCLs) {
      if(currBranch.containsFormula(f.getNegation())) {
        currBranch.addDeduction(new ModalFormula("⊥"), "(cl)");
        return true;
      }
    }

    return false;
  }

  /**
   * Remove the double negation from the front of currFormula
   */
  public boolean notNot(TableauFormula f) {
    ModalFormula withoutNegations = f.getSubformula().getSubformula();
    return currBranch.addDeduction(new TableauFormula(f.getWorld(), withoutNegations),
                                   "(¬¬)");
  }

  /**
   * Add all conjuncts in the given conjunction to the branch
   */
  public boolean alpha(TableauFormula f) {
    boolean addedDeduction = false;
    for(ModalFormula sub : f.getSubformula()
                                     .getSubformulae()){
      World conjunctWorld = f.getWorld();
      ModalFormula conjunct = sub.getComplement();
      TableauFormula conjunctInWorld = new TableauFormula(conjunctWorld, conjunct);
      addedDeduction = addedDeduction | currBranch.addDeduction(conjunctInWorld, "(α)");
                                      
    }
   return addedDeduction;
  }

  /**
   * Split on disjunction.
   * Put each distjunct in a seperate branch, adding the new branches
   * to the tableau.
   */
  public void beta(TableauFormula disjunction) {
    World disjWorld = disjunction.getWorld();
    ArrayList<ModalFormula> disjuncts = disjunction.getSubformulae();
    int nDisjuncts = disjuncts.size();

    //for beta rule, break off first disjunct for this branch,
    //push rest onto new branch
    ModalFormula firstDisjunct = disjuncts.get(0);

    //duplicate disjuncts for other branch
    ArrayList<ModalFormula> otherDisjuncts = new ArrayList<ModalFormula>();
    for(int i=1; i<nDisjuncts; i++)
      otherDisjuncts.add(disjuncts.get(i).copyOf());
    ModalFormula restOfDisjunction = new ModalFormula(Operation.OR, otherDisjuncts);
    TableauFormula newDisjunction = new TableauFormula(disjWorld, restOfDisjunction);

    Branch newBranch = new Branch(currBranch);

    currBranch.addDeduction(new TableauFormula(disjWorld, firstDisjunct), "(β)");
    
    addBranch(newBranch, newDisjunction, "(β)");
    
  }

  /**
   * Attempt a single box rule application.
   * given a: box(p) and R(a, b)
   * deduce b: p
   */
  public boolean box() {
    //System.out.println("considering box deduction");
    if(currBranch.getBoxFormulae().isEmpty())  return false;

    for(TableauFormula boxF : currBranch.getBoxFormulae()){
      ModalFormula necessary = boxF.getSubformula();
      World from = boxF.getWorld();
      for(WorldRelation rel : currBranch.getRelatedWorlds(from)) {
        World to = rel.getSecond();
        TableauFormula boxConsequence = new TableauFormula(to, necessary);
        //if(currBranch.addDeduction(boxConsequence, "([])")){
        if(currBranch.addDeduction(boxConsequence, "(◻)")){
          boxF.setRespectedIn(to);
          countBox++;
          return true;
        }
      }
    }
    
    return false;
  }
  
  /**
   * Apply the notBox rule.
   * Suppose a new ancestor world where the subformula is true.
   * Track the rewrite of this new witnessing world.
   */
  public boolean notBox() {
    if(currBranch.getDiamondFormulae().isEmpty())  return false;
    
    //System.out.println(currBranch.getDiamondFormulae());
    TableauFormula diaF = currBranch.getDiamondFormulae().pop();
    ModalFormula possible = diaF.getSubformula().getSubformula().getComplement();

    //don't apply notBox if it is already satisfied in some ancestor worlds
    for(WorldRelation wr : currBranch.getRelatedWorlds(diaF.getWorld()))
    {
      TableauFormula possibleDuplicate = new TableauFormula(wr.getSecond(), possible);
      if(currBranch.containsFormula(possibleDuplicate))
        return false;
    } 

    if(currBranch.isENormal(diaF.getWorld())) {
      //apply (¬◻)
        KTerm to = new KTerm();
        String witnessLabel = "f(" + diaF.getModalFormula() + ")";
        World witness = new WitnessWorld(witnessLabel, diaF.getWorld());
        DRule rule = new DRule(witness, to);
        World from = diaF.getWorld();
        
        currBranch.addDeduction(new WorldRelation(from, to), "(¬◻)");


        currBranch.addDeduction(new TableauFormula(to, possible),
                                      "(¬◻)");
        currBranch.addDeduction(rule, "(¬◻)");
        diaF.setRespectedIn(to);
        countNotBox++;
        return true;
      }
    
    return false;
  }
  
  /**
   * Unrestricted blocking.
   * Assume the equality of two worlds on the current branch.
   */
  public boolean ub() {
    if(!blocking)
      return false;

    DeductionSet<World> worldsForBlocking = currBranch.getWorlds();

    //we can only do equality reasoning on 2 or more worlds
    if(worldsForBlocking.size() < 2)
      return false;

    DeductionSet<World> worlds = currBranch.getWorlds();

    //pick s t, distinct and e normal
    World s = new World("!"), t = new World("!"); //TODO replace this
    boolean foundNewEquivalence = false;
    //System.out.println(currBranch.getWorldEqualities());
    //System.out.println(currBranch.getWorlds());
    for(World w1 : worlds) {
      for(World w2 : worlds) {
        //System.out.printf("enormal of w1=%s is %s\n", w1, currBranch.getENormal(w1));
        //System.out.printf("enormal of w2=%s is %s\n", w2, currBranch.getENormal(w2));
        if(
           !w1.equals(w2) &&
           !currBranch.getWorldEqualities().contains(new WorldEquality(w1, w2, false))) {
          s=w1;
          t=w2;

          //System.out.printf("s=%s t=%s\n", s, t);
          //System.out.println(w1 instanceof KTerm && w2 instanceof KTerm);
          if((w1 instanceof KTerm && w2 instanceof KTerm)
              && (currBranch.getCRules().contains(new CRule((KTerm)w1, (KTerm)w2))
                  || currBranch.getCRules().contains(new CRule((KTerm)w2, (KTerm)w1))))
            continue;
          //System.out.printf("s=%s t=%s\n", s, t);

          WorldEquality eq = new WorldEquality(s, t, true),
          neq = new WorldEquality(s, t, false);
          //System.out.println("new eq " + eq);
          //create s eqv t on one branch, s neqv t on another
          Branch newBranch = new Branch(currBranch);
          currBranch.addDeduction(eq, "(ub)");
          //System.out.println("first after split:\n" + currBranch.getWorldEqualities() + currBranch.getWorlds());
          addBranch(newBranch, neq, "(ub)");
          //System.out.println("second after split:\n" + newBranch.getWorldEqualities() + newBranch.getWorlds());
          countUB++;
          return true;
        }
      }
    }
    
    return false;
  }
  
  //frame condition tableau rules

  /**
   * Refelxivity.
   * Deduce a singe reflexive relation R(a,a) for a world a.
   */
  public boolean refl() {
    //check if frame condition is allowed
    if (!frameConditions.contains("reflexive"))  return false;

    for(World w : currBranch.getWorlds())
      if(currBranch.addDeduction(new WorldRelation(w, w), "(refl)"))
        return true;

    return false;
  }

  /**
   * Symmetry.
   * For some relation R(a,b) deduce its symmetric reflection R(b,a).
   */
  public boolean sym() {
    //check if frame condition is allowed
    if (!frameConditions.contains("symmetric"))  return false;

    for(WorldRelation wr : currBranch.getWorldRelations())
    {
      World first = wr.getFirst(),
            second = wr.getSecond();
      WorldRelation symmetric = new WorldRelation(second, first);

      if(currBranch.addDeduction(symmetric, "(sym)"))
        return true;
    }

    return false;
  }

  /**
   * Transitivity.
   * Given R(a,b) and R(b,c) deduce R(a,c).
   */
  public boolean tr() {
    //check if frame condition is allowed
    if (!frameConditions.contains("transitive"))  return false;

    for(WorldRelation r1 : currBranch.getWorldRelations())
    {
      for(WorldRelation r2 : currBranch.getRelatedWorlds(r1.getSecond()))
      {
        WorldRelation transitive = new WorldRelation(r1.getFirst(), r2.getSecond());
        if(currBranch.addDeduction(transitive, "(tr)"))
          return true;
      }
    }

    return false;
  }

  /**
   * Seriality.
   * Given a world suppose it has an ancestor.
   */
  public boolean serial() {
    //check if frame condition is allowed
    if (!frameConditions.contains("serial"))  return false;

    for(World w : currBranch.getWorlds()) {
      for(WorldRelation r : currBranch.getWorldRelations()) {
        if(r.getFirst().equals(w))  continue;
        else {
          World successor = new KTerm();
          currBranch.addDeduction(new WorldRelation(w, successor), "(serial)");
          return true;
        }
      }
    }

    return false;
  }
  
  //These three for the single distinct predecessor world relation
  /**
   * 1.
   * Create the predecessor.
   */
  public boolean dp1() {
    //check if frame condition is allowed
    if (!frameConditions.contains("single distinct predecessor"))  return false;

    for(World w : currBranch.getWorlds()) {
      if(currBranch.getENormal(w).equals(w) && !w.hasDPPredecessor()) {
        KTerm d = new KTerm();
        currBranch.addDeduction(new WorldRelation(d, w), "(dp1)");
        WitnessWorld wit = new WitnessWorld("g", w);
        if(currBranch.addDeduction(new DRule(wit, d), "(dp1)")) {
          w.setHasDPPredecessor();
          return true;
        }
      }
    }
    return false;
  }
  
  /**
   * 2.
   * Close if we have assumed a world is its own predecessor.
   */
  public boolean dp2() {
    //check if frame condition is allowed
    if (!frameConditions.contains("single distinct predecessor"))  return false;

    for(DRule dRule : currBranch.getDRules()) {
      if(dRule.getFirst() instanceof WitnessWorld &&
         ((WitnessWorld)dRule.getFirst()).getWitnessLabel().charAt(0) == 'g') {
        WitnessWorld wit = (WitnessWorld) dRule.getFirst();
        if(currBranch.getENormal(dRule.getSecond()).equals(wit.witnessOf())) {
          return currBranch.addDeduction(new ModalFormula("⊥"), "(dp2) " + wit + " " + dRule.getSecond());
        }
      }
    }
    return false;
  }

  /**
   * 3.
   * Given a chain of three worlds,
   * assume on one branch that the first two are equal
   * and on another branch that the second two are equal
   */
  public boolean dp3() {
    //check if frame condition is allowed
    if (!frameConditions.contains("single distinct predecessor"))  return false;

    World c = null, cDash = null, d = null, dDash = null;
    boolean foundADp3 = false;
    for(DRule rule : currBranch.getDRules()) {
      if(foundADp3)  break;
      if(rule.getFirst() instanceof WitnessWorld &&
          ((WitnessWorld)rule.getFirst()).getWitnessLabel().charAt(0) == 'g') {
        c = ((WitnessWorld) rule.getFirst()).witnessOf();
        dDash = rule.getSecond();
        //find R(d,c') and R(c',c)
        for(WorldRelation r2 : currBranch.getWorldRelations()) {
          if(foundADp3)  break;
          if(r2.getSecond().equals(c)) {
            cDash = r2.getFirst();
            for(WorldRelation r1 : currBranch.getWorldRelations()) {
              if(!r1.equals(r2) &&
                 r1.getSecond().equals(cDash) &&
                 currBranch.getENormal(dDash).equals(r1.getFirst())) {
                foundADp3 = true;
                d = r1.getFirst();
                dDash = rule.getSecond();
              }
            }
          }
        }
      }
    }
    
    if(foundADp3) {
      Branch newBranch = new Branch(currBranch);
      currBranch.addDeduction(new WorldEquality(c, cDash, true), "(dp3) on d=" + d + ", c\'=" + cDash + ", c=" + c + ", d\'=" + dDash);
      Deduction newDed = new WorldEquality(cDash, d, true);
      String newDedReason = "(dp3)";
      addBranch(newBranch, newDed, newDedReason);
      return true;
    }
    
    return false;
  }

  /**
   * Add some new branch to our tableau.
   * Called when a splitting rule is applied.
   * Modelled as a stack so when a branch is closed, we move to the next lowest
   * splitting point.
   */
  public boolean addBranch(Branch b, Deduction d, String deductionReason){
    push(b);
    newBranchDeductions.push(d);
    newBranchDeductionReasons.push(deductionReason);

    System.out.print("branches: [");
    for(Branch branch : this)
      System.out.print(" " + branch.getBranchID());
    System.out.print(" ]\n");
    return true;
  }

  /**
   * Obtain the next branch in the tableau derivation.
   * Called after a branch has been closed. The deduction is made at this point
   * so it reads in a sensible order in the derivation output.
   */
  public Branch getNextBranch() {
    Branch nextBranch = pop();

    //when moving to a new branch,
    //add the alternative deduction from the split
    if(!newBranchDeductions.isEmpty()) {
      Deduction branchDeduction = newBranchDeductions.pop();
      nextBranch.addDeduction(branchDeduction,
                              newBranchDeductionReasons.pop());
    }

    return nextBranch;
  }

  /**
   * @return true iff the given branch satisfies the tableau conditions.
   */
  private boolean branchGivesInterpretation(Branch b)
  {
    return (b.interpretsAllModalFormulae()
        && !b.isClosed());
  }
}
