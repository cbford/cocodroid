package portfolio.connorford.cocotab.tableau;

import java.util.ArrayDeque;

import portfolio.connorford.cocotab.congruenceClosure.KTerm;
import portfolio.connorford.cocotab.logic.World;

/**
 * Represents a collection of deductions that contains no duplicates.
 * A double ended queue, to allow for different access.
 */
@SuppressWarnings("serial")
public class DeductionSet<D extends Deduction> extends ArrayDeque<D> {
  /**
   * Add a deduction iff it is not already in the set
   *
   * @return true iff a new deduction is added
   */
  public boolean offer(D d) {
    if(contains(d))
      return false;
    else {
      add(d);
      return true;
    }
  }
  
  /**
   * Add a deduction iff it is not already in the set
   *
   * @return true iff a new deduction is added
   */
  public boolean offerFirst(D d) {
    if(contains(d))
      return false;
    else {
      addFirst(d);
      return true;
    }
  }
  
  /**
   * Extract and return the first in the deque, move it to the back.
   *
   * @return the deduction at the front of the queue
   * @throws NullPointerException if the structure is empty
   */
  public D pick() {
    D ret = pop();
    addLast(ret);
    return ret;
  }
  
  /**
   * World rewriting for every deduction in the set
   *
  /*@return true iff any are changed
   */
  public boolean rewriteWorld(World from, World to, String reason) {
    boolean didARewrite = false;
    for(Deduction deduction : this)
      if(deduction.rewriteWorld(from, to, reason)) {
        System.out.println(deduction + "\t rewrite " + from
                           + " as " + to + " by " + reason);
        didARewrite = true;
      }
    
    return didARewrite;
  }
  
  public String toString() {
    if(isEmpty())
      return "[]\n";
    
    String ret = "";
    ret = super.toString() + "\n";
    return ret;
  }
  
  /**
   * Deep copy for branch splitting
   */
  public DeductionSet<D> copyOf() {
    DeductionSet<D> ds = new DeductionSet<D>();
    for(D d : this) {
      if(d instanceof KTerm)
        ds.add((D)((KTerm)d).copyOf());
      else
        ds.add((D) d.copyOf());
    }

    return ds;
  }

  /**
   * Remove any duplicated deductions.
   * ensures the structure behaves like a set.
   */
  public void removeDuplicates() {
    DeductionSet<D> ret = new DeductionSet<D>();
    ret.addAll(this);
    this.clear();
    for(D ded : ret){
      offer(ded);
    }
  }
}
