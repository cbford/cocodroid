package portfolio.connorford.cocotab.tableau;
import portfolio.connorford.cocotab.logic.World;

/**
 * Represents the consequence of applying some rule in a tableau
 */
public abstract class Deduction{

    /**
     * Rewrite a world occuring in a deduction.
     * As the consequence of a conguence closure rule. Some deductions cannot
     * be rewritten.
     *
     * @return true iff a valid rewrite is performed
     */
    public abstract boolean rewriteWorld(World from, World to, String reason);

    /**
     * Create a deep copy.
     * a deduction may be cloned when a branch splits
     */
    public abstract Deduction copyOf();
}
