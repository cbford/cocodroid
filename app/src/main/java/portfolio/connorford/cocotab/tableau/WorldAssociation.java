package portfolio.connorford.cocotab.tableau;

import portfolio.connorford.cocotab.logic.World;

/**
 * Represents a binary relation between two worlds.
 * Can be an equality or a relation.
 */
public abstract class WorldAssociation extends Deduction{

  protected World first, second;

  /**
   * Class constructor.
   * For a fresh associations
   */
  public WorldAssociation(World w1, World w2) {
    first = w1;
    second = w2;
  }

  //getters and setters for each world in the association
  public World getFirst() {
    return first;
  }

  public World getSecond() {
    return second;
  }
  
  public void setFirst(World f) {
    first = f;
  }

  public void setSecond(World f) {
    second = f;
  }
  
  /**
   * Get unique identifier for the association.
   * associations are compared by hashcode
   * the hashcode is determined by
   * a unique integer paring function, pairing the world hashcodes
   */
  public int hashCode() {
    int w1 = first.hashCode(),
        w2 = second.hashCode();
    
    return ((w1+w2)*(w1+w2+1)/2) + w1;
  }

  /**
   * Rewrite terms in the association.
   *
   * @return true iff a rewrite is done on either term or a subterm of either
   */
  public boolean rewriteWorld(World from, World to, String reason) {
    boolean didRewrite = false;
    if(first.equals(from)) {
      first = to;
      didRewrite = true;
    }
    
    if(second.equals(from)) {
      second = to;
      didRewrite = true;
    }

    if (first.rewriteWorld(from, to, reason) | second.rewriteWorld(from, to, reason)){
      //handle witness subterm rewriting
      didRewrite = true;
    }
    return didRewrite;
  }
  
  public abstract String toString();
}
