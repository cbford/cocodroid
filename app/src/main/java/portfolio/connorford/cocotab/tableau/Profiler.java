package portfolio.connorford.cocotab.tableau;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Profiler
{
  static private FileHandler fileTxt;
  static private SimpleFormatter formatterTxt;

  static public void setUp(String logFileName) throws IOException
  {
    System.setProperty("java.util.logging.SimpleFormatter.format", "%5$s%n");
    //get global logger for configuration
    Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    //info level logs are made in the derivation algorithm
    logger.setLevel(Level.INFO);
    //logger handler is currently console output

    //suppress console output from logger
//    Logger rootLogger = Logger.getLogger("");
//    Handler[] handlers = rootLogger.getHandlers();
//    if(handlers[0] instanceof ConsoleHandler)
//    {
//      rootLogger.removeHandler(handlers[0]);
//    }

    //add file txt logger
//    logger.setLevel(Level.INFO);
//    //fileTxt = new FileHandler(logFileName, false); //boolean for append mode,
//    fileTxt = new FileHandler("cocotabLog.txt", false); //boolean for append mode
//
//    formatterTxt = new SimpleFormatter();
//
//    fileTxt.setFormatter(formatterTxt);
//    logger.addHandler(fileTxt);
  }
}
