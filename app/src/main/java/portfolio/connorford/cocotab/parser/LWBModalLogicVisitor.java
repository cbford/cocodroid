package portfolio.connorford.cocotab.parser;
// Generated from ./src/parser/LWBModalLogic.g4 by ANTLR 4.5

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link LWBModalLogicParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface LWBModalLogicVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link LWBModalLogicParser#start}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStart(LWBModalLogicParser.StartContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Binary}
	 * labeled alternative in {@link LWBModalLogicParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinary(LWBModalLogicParser.BinaryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Nest}
	 * labeled alternative in {@link LWBModalLogicParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNest(LWBModalLogicParser.NestContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Unary}
	 * labeled alternative in {@link LWBModalLogicParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary(LWBModalLogicParser.UnaryContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Atom}
	 * labeled alternative in {@link LWBModalLogicParser#formula}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtom(LWBModalLogicParser.AtomContext ctx);
}
