package portfolio.connorford.cocotab.parser;
// Generated from ./src/parser/LWBModalLogic.g4 by ANTLR 4.5

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.RuntimeMetaData;
import org.antlr.v4.runtime.Vocabulary;
import org.antlr.v4.runtime.VocabularyImpl;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.LexerATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LWBModalLogicLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, UN=3, BN=4, ATOM=5, WS=6;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "UN", "BN", "ATOM", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'('", "')'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, "UN", "BN", "ATOM", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public LWBModalLogicLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "LWBModalLogic.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\b<\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\3\2\3\2\3\3\3\3\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\5\4\33\n\4\3\5\3\5\3\5\3\5\3\5\3\5\5\5#\n\5\3\6\3\6\6\6\'"+
		"\n\6\r\6\16\6(\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\64\n\6\3\7\6\7"+
		"\67\n\7\r\7\16\78\3\7\3\7\2\2\b\3\3\5\4\7\5\t\6\13\7\r\b\3\2\5\4\2((x"+
		"x\3\2\62;\5\2\13\f\17\17\"\"C\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t"+
		"\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\3\17\3\2\2\2\5\21\3\2\2\2\7\32\3\2\2"+
		"\2\t\"\3\2\2\2\13\63\3\2\2\2\r\66\3\2\2\2\17\20\7*\2\2\20\4\3\2\2\2\21"+
		"\22\7+\2\2\22\6\3\2\2\2\23\33\7\u0080\2\2\24\25\7d\2\2\25\26\7q\2\2\26"+
		"\33\7z\2\2\27\30\7f\2\2\30\31\7k\2\2\31\33\7c\2\2\32\23\3\2\2\2\32\24"+
		"\3\2\2\2\32\27\3\2\2\2\33\b\3\2\2\2\34#\t\2\2\2\35\36\7/\2\2\36#\7@\2"+
		"\2\37 \7>\2\2 !\7/\2\2!#\7@\2\2\"\34\3\2\2\2\"\35\3\2\2\2\"\37\3\2\2\2"+
		"#\n\3\2\2\2$&\7r\2\2%\'\t\3\2\2&%\3\2\2\2\'(\3\2\2\2(&\3\2\2\2()\3\2\2"+
		"\2)\64\3\2\2\2*+\7v\2\2+,\7t\2\2,-\7w\2\2-\64\7g\2\2./\7h\2\2/\60\7c\2"+
		"\2\60\61\7n\2\2\61\62\7u\2\2\62\64\7g\2\2\63$\3\2\2\2\63*\3\2\2\2\63."+
		"\3\2\2\2\64\f\3\2\2\2\65\67\t\4\2\2\66\65\3\2\2\2\678\3\2\2\28\66\3\2"+
		"\2\289\3\2\2\29:\3\2\2\2:;\b\7\2\2;\16\3\2\2\2\b\2\32\"(\638\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}
