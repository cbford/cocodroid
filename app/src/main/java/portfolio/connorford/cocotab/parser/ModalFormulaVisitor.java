package portfolio.connorford.cocotab.parser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.ArrayList;

import portfolio.connorford.cocotab.logic.Operation;
import portfolio.connorford.cocotab.logic.ModalFormula;

//the visits the parse tree of an LWB formula
// and constructs a ModalFormula, using only
// the operators in the tableau system:
//  ~, v, box

public class ModalFormulaVisitor
             extends LWBModalLogicBaseVisitor<ModalFormula> {
	
	@Override
	public ModalFormula visitBinary(LWBModalLogicParser.BinaryContext ctx) {
		Operation or = Operation.OR;
		ArrayList<ModalFormula> sf = new ArrayList<ModalFormula>();
    LWBModalLogicParser.FormulaContext leftFormula = ctx.formula(0),
            rightFormula = ctx.formula(1);
    String operatorLabel = ctx.getChild(1).getText();
		//determine the specific operation
		switch(operatorLabel) {
		case "&": //a&b is ~(~a v ~b)
      sf.add(visit(leftFormula).getComplement());
      sf.add(visit(rightFormula).getComplement());
			return new ModalFormula(Operation.NOT, new ModalFormula(or, sf));
		case "v": //avb is allowed
			sf.add(visit(leftFormula));
	    sf.add(visit(rightFormula));
			return new ModalFormula(or, sf);
		case "->": //a->b is ~a v b
		  sf.add(visit(leftFormula).getComplement());
		  sf.add(visit(rightFormula));
		  return new ModalFormula(or, sf);
		case "<->": //a<->b is (~a v b)&(a v ~b) is t1 & t2 is ~(~t1 v ~t2)
		  ModalFormula t1, t2;
		  sf.add(visit(leftFormula).getComplement());
      sf.add(visit(rightFormula));
		  t1 = new ModalFormula(or, sf);
		  sf = new ArrayList<ModalFormula>();
		  sf.add(visit(leftFormula));
      sf.add(visit(rightFormula).getComplement());
      t2 = new ModalFormula(or, sf);
      sf = new ArrayList<ModalFormula>();
      sf.add(t1.getComplement());
      sf.add(t2.getComplement());
      return new ModalFormula(Operation.NOT, new ModalFormula(or, sf));
		default:
      System.out.println("Didn't recognise operator label: " + operatorLabel);
      System.exit(1);
      return null;
		}
	}

	@Override
	public ModalFormula visitNest(LWBModalLogicParser.NestContext ctx) {
		return visit(ctx.formula());
	}

	@Override
	public ModalFormula visitUnary(LWBModalLogicParser.UnaryContext ctx) {
		Operation op;
    ModalFormula sub = visit(ctx.formula());
    String opLabel = ctx.getChild(0).getText();

		//determine the specific operation
		switch(opLabel) {
		case "~": //allowed
      //remove double negations
      if(sub.getOperator().equals(Operation.NOT)){
        return sub.getComplement();
      }
      else
        op = Operation.NOT;
      break;
		case "box": //allowed
		  	op = Operation.BOX;
        break;
		case "dia": //dia(a) is ~(box(~a)
			return new ModalFormula(Operation.NOT, 
			                        new ModalFormula(Operation.BOX, sub.getComplement()));
		default:
		//TODO: this may only happen if the grammar definition is messed up
      //should throw an exception
      //returns ~a for now
			op = Operation.NOT;
			break;
		}
		
		return new ModalFormula(op, sub);
	}

	@Override
	public ModalFormula visitAtom(LWBModalLogicParser.AtomContext ctx) {
	  if(ctx.getChild(0).getText().equals("false"))
	    return new ModalFormula("⊥");
	  else if (ctx.getChild(0).getText().equals("true"))
	    return new ModalFormula("T");
	  else
	    return new ModalFormula(ctx.getText());
	}
	
	public ModalFormula getFormula(String formulaSentence) {
		LWBModalLogicLexer lexer = new LWBModalLogicLexer(
                                        new ANTLRInputStream(formulaSentence));
		CommonTokenStream tokens =new CommonTokenStream(lexer);
		LWBModalLogicParser parser = new LWBModalLogicParser(tokens);
		ParseTree tree = parser.start();
	  ModalFormula mf = new ModalFormulaVisitor().visit(tree.getChild(0));
	    
	    return mf;
	}
	
}
