package portfolio.connorford.cocotab.logic;

import java.util.ArrayList;

import portfolio.connorford.cocotab.tableau.Deduction;
import portfolio.connorford.cocotab.tableau.DeductionSet;

/**
 * Represents a formula in modal logic that does not have a world labelling it
 */
public class ModalFormula extends Deduction {
	private Operation operator;
	private ArrayList<ModalFormula> subformulae;
  private DeductionSet<ModalFormula> atomsToInterpret;
	private String atomLabel;

  // these two methods for deep copy for branch splitting
  private ModalFormula(ModalFormula mf) {
    this.operator = mf.operator;

    //deep copy of subformulae
    switch(operator) {
      case NONE:
      case NOT:
      case BOX:
        this.subformulae = new ArrayList<ModalFormula>();
        this.subformulae.add(mf.getSubformula());
        break;
      case OR:
        this.subformulae = new ArrayList<ModalFormula>();
        for(ModalFormula mfToCopy : mf.subformulae)
          this.subformulae.add(mfToCopy.copyOf());
        break;
      default:
        throw new RuntimeException("tried to copy " + this
                                   + "but couldnt identify operator");
    }

    this.atomLabel = mf.atomLabel;
    this.atomsToInterpret = mf.getAtomsToInterpret();
  }
	
  public ModalFormula copyOf() {
    return new ModalFormula(this);
  }

	/*
   * Constructor for an atomic formula
   */
	public ModalFormula(String reqAtomLabel) {
		operator = Operation.NONE;
		atomLabel = reqAtomLabel;
    atomsToInterpret = new DeductionSet<ModalFormula>();
    if(!atomLabel.equals("T") && !atomLabel.equals("⊥")) {
          atomsToInterpret.add(this);
       }
	}
	
	/**
   * Constructor for a formula with a unary operator
   */
	public ModalFormula(Operation o, ModalFormula p) {
    atomsToInterpret = p.getAtomsToInterpret();

    //formula constructed with respect to its operator
		switch(o){
		case NOT:
		  operator = o;
      subformulae = new ArrayList<ModalFormula>();
      //handle ~⊥ = T and visa versa
      if(p.equals(new ModalFormula("T"))) {
        operator = Operation.NONE;
        atomLabel = "⊥";
        }
      else if(p.equals(new ModalFormula("⊥"))) {
        operator = Operation.NONE;
        atomLabel = "T";
      }
      else if(p.getOperator() == Operation.OR && p.subformulae.size() == 1){
        //we have ((f)v(empty disjuction)), so replace with (f)
        operator = o;
        subformulae.add(p.subformulae.get(0));
      }
      else
        subformulae.add(p);
      break;
		case BOX:
			operator = o;
			subformulae = new ArrayList<ModalFormula>();
			subformulae.add(p);
			break;
		default:
      //operator provided is not unary
      throw new IllegalArgumentException(
				 "A formula must be given one subformula only if its operator is unary.");
		}
	}
	
	/**
   * Constructor for a nonunary operator
   */
	public ModalFormula(Operation o, ArrayList<ModalFormula> pf) {
    atomsToInterpret = new DeductionSet<ModalFormula>();
    for(ModalFormula f : pf)
      atomsToInterpret.addAll(f.getAtomsToInterpret());

    //formula constructed with respect to its operator
		switch(o){
		case OR:
			operator = o;
			subformulae = new ArrayList<ModalFormula>();
			//flatten disjunction: send av(bvc) to avbvc
			//send (avT) to T
			for(ModalFormula sub : pf) {
			  if(sub.getOperator() == Operation.OR)
			    subformulae.addAll(sub.getSubformulae());
			  else
			    subformulae.add(sub);
			}
			
			for(ModalFormula sub : subformulae) {
			  if(sub.getOperator() == Operation.NONE
	           && sub.equals(new ModalFormula("T"))) {
	          operator = Operation.NONE;
	          atomLabel = "T";
	        }
			}
			break;
		default:
      //unexpected operator
      throw new IllegalArgumentException(
        "Unexpected operator " + o
        + " given to nonunary ModalFormula constructor");
		}
	}
	
	public String toString() {
		String ret = "(";
		switch(operator){
		case NOT:
		case BOX:
			ret += operator + subformulae.get(0).toString() + ")";
			break;
		case NONE:
			ret += atomLabel + ")";
			break;
		case OR:
			for(ModalFormula p : subformulae) {
				ret += p;
				ret += operator;
			}
			ret = ret.substring(0, ret.length()-1) + ")";
			break;
		default:
			throw new IllegalArgumentException();
		}
		return ret;
	}

	/**
   * @return the subformulae under this nonunary operation
   */
	public ArrayList<ModalFormula> getSubformulae() {
		switch(operator) {
		case OR:
			return subformulae;
		default:
      //called for a formulae with unexpected operator
			throw new RuntimeException("Tried to getSubformulae() on " + this);
		}
	}
	
	/**
   * @return the subformula under this unary operation
   */
	public ModalFormula getSubformula() {
		switch(operator) {
		case NOT:
		case BOX:
			return subformulae.get(0);
		case NONE:
		  return this;
		default:
      //called for a formulae with unexpected operator
			throw new RuntimeException("Tried to getSubformula() on " + this);
		}
		
	}
	
	public Operation getOperator() {
		return operator;
	}
	
	public String getAtom() {
		if(isAtomic())
			return atomLabel;
		else
			throw new RuntimeException("tried to getAtom on " + this);
	}
	
  /**
   * @return true iff of the form p or not(p)
   *         where p is a proposition variable
   */
	public boolean isAtomic() {
	  if(operator == Operation.NONE
	      || (operator == Operation.NOT && getSubformula().getOperator() == Operation.NONE))
	    return true;
	  else
	    return false;
	}
	
	public boolean equals(Object o) {
		if(o instanceof ModalFormula) {
			//System.out.println("ModalFormula comparison: " + this + " = " + o + "?");
			return this.toString().equals(o.toString());
		}
		return false;
	}

  public int hashCode() {
    return toString().hashCode();
  }

  //We may not rewrite formulae
  public boolean rewriteWorld(World from, World to, String reason) {
    return false;
  }

  /**
   * @return the atoms contained in the subformalae of this
   */
  public DeductionSet<ModalFormula> getAtomsToInterpret() {
    atomsToInterpret.removeDuplicates();
    return atomsToInterpret;
  }

  /**
   * @return ~p := ~p = q, if p = ¬q
   *               ~p = ¬q otherwise
  */
  public ModalFormula getComplement() {
    ModalFormula complement;

    if(this.operator == Operation.NOT) {
      complement = subformulae.get(0);
    }
    else {
      complement = new ModalFormula(Operation.NOT, this);
    }

   // System.out.printf("complement of %s is %s\n", this, complement); 
    
    return complement;
  }
}
