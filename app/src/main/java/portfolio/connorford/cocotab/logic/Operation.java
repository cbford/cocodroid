package portfolio.connorford.cocotab.logic;

/**
 * Represents a logical operation over a formula in modal logic
 */
public enum Operation {
	NONE,
	NOT,
	BOX,
	OR;
	
	@Override
  //chars may not be supported
	public String toString() {
		switch(this) {
		case NONE: return "";
		case NOT: return "¬";
		case BOX: return "◻";
		case OR: return "∨";
		default: throw new IllegalArgumentException();
		}
	}
}
