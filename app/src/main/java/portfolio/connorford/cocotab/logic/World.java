package portfolio.connorford.cocotab.logic;

import portfolio.connorford.cocotab.tableau.Deduction;
import portfolio.connorford.cocotab.congruenceClosure.KTerm;
import portfolio.connorford.cocotab.congruenceClosure.WitnessWorld;

/**
 * Represents a term that labels some formulae
 */
public class World extends Deduction implements Comparable<World>{
  protected String label;
  protected boolean eNormal;
  protected boolean hasDPPredecessor;

  //for deep copying to  new branches
  public World(World w) {
    this.label = w.label;
    this.eNormal = w.eNormal;
    this.hasDPPredecessor = w.hasDPPredecessor;
  }

  public World copyOf() {
    return new World(this);
  }
  
  /**
   * Class contructor for a fresh world
   */
  public World(String reqLabel) {
    label = reqLabel;
    eNormal = false;
    hasDPPredecessor = false;
  }

  //methods to prevent duplicate predecessor worlds
  //when tablaue has dp frame condition
  public boolean hasDPPredecessor() {
    return hasDPPredecessor;
  }
  
  public void setHasDPPredecessor() {
    hasDPPredecessor = true;
  }

   //The smallest rewritable subterm in a general world is the world itself
  public World getSmallestSubterm() {
    return this;
  }

  public String toString() {
    return label;
  }
  
  public boolean equals(Object o) {
    if(o instanceof World) {
      return hashCode() == ((World)o).hashCode();
    }
    return false;
  }
  
  public int hashCode() { //general worlds are greater than Kterms and witness worlds
    return Math.abs(label.hashCode()%1000 + KTerm.getMaxTermCount() + WitnessWorld.getMaxTermCount());
  }

  @Override
  //we don't overwrite generic worlds themselves
  public boolean rewriteWorld(World from, World to, String reason) {
    return false;
  }
  
  @Override
  public int compareTo(World o) {
    if(o instanceof KTerm || o instanceof WitnessWorld)
      return 1;
    else
      return this.hashCode() - o.hashCode();
//    if(this instanceof KTerm && !(w instanceof KTerm))
//      return -1;
//    else if(w instanceof WitnessWorld)
//      return -1*w.compareTo(this);
//
//    return this.hashCode() - w.hashCode();
  }
}
