package portfolio.connorford.cocotab.logic;

import java.util.ArrayDeque;
import java.util.ArrayList;

import portfolio.connorford.cocotab.tableau.Deduction;
import portfolio.connorford.cocotab.tableau.DeductionSet;
import portfolio.connorford.cocotab.parser.ModalFormulaVisitor;

/**
 * Represents a ModalFormula labelled with a world in a tableau
 */
public class TableauFormula extends Deduction{
  ModalFormula mf;
  World w;
  DeductionSet<World> respectedIn;
  
  /**
   * Class constructor for input formulae
   *
   * @param worldLabel  The world in which the formula holds
   * @param formulaSentence  The string from which we construct a modal formula
   */
  public TableauFormula(String worldLabel, String formulaSentence) {
    w = new World(worldLabel);
    mf = new ModalFormulaVisitor().getFormula(formulaSentence);
    respectedIn = new DeductionSet<World>();
  }
  
  /**
   * Class constructor for newly derived formulae
   */
  public TableauFormula(World world, ModalFormula formula) {
    w = world;
    mf = formula;
    respectedIn = new DeductionSet<World>();
  }
  
  public World getWorld() {
    return w;
  }
  
  /**
   * @return the unlabelled formula
   */
  public ModalFormula getModalFormula() {
    return mf;
  }
  
  public String toString() {
    return w + ": " + mf;
  }
  
  /**
   * Encapsulated version of method in ModalFormula class.
   *
   * @return the subformulae of the unlabelled formula
   */
  public ArrayList<ModalFormula> getSubformulae() {
    return mf.getSubformulae();
  }
  
  /**
   * Encapsulated version of method in ModalFormula class.
   *
   * @return the subformula of the unlabelled formula
   */
  public ModalFormula getSubformula() {
    return mf.getSubformula();
  }
  
  /**
   * Encapsulated version of method in ModalFormula class.
   * name changed for readibility in the code
   *
   * @return the subformula of the unlabelled formula
   */
  public TableauFormula getNegation() {
    return new TableauFormula(w, mf.getComplement());
  }

  /**
   * Set the respected world of a diamond formuale
   */
  public void setRespectedIn(World w) {
    respectedIn.offer(w);
    //System.out.println("respected " + this);
  }

  public boolean isRespectedIn(World w) {
    return respectedIn.contains(w);
  }

  public ArrayDeque<World> getRespectedIn() {
    return respectedIn;
  }
  
  @Override
  //uniquely determined by world and mf
  public int hashCode() {
    int t1 = w.hashCode(),
        t2 = mf.toString().hashCode();
    //System.out.println(this +  "t1: " + t1 + " t2: " + t2);
    
    return ((t1+t2)*(t1+t2+1)/2) + t1;
  }

  
  @Override
  public boolean equals(Object o) {
    if(o instanceof TableauFormula) {
      TableauFormula other = (TableauFormula) o;
      //System.out.println(this + " " + this.hashCode() + " ?= "+ other.hashCode() + " " + other);
      return this.w.equals(other.w) && this.mf.toString().equals(other.mf.toString());
      //return this.hashCode() == ((TableauFormula)o).hashCode();
    }
    return false;
  }

  @Override
  /**
   * rewrite all occurences of world from as world to in this object
   */
  public boolean rewriteWorld(World from, World to, String reason) {
    boolean didARewrite = false;
    if(w.equals(from)) {
      w = to;
      didARewrite = true;
    }
    else if (w.rewriteWorld(from, to, reason)){
      //handle witness subterm rewriting
      didARewrite = true;
    }

    respectedIn.remove(from);
    respectedIn.offer(to);

    return didARewrite;
  }

  // these methods for deep copying formulae to new split branch
  public TableauFormula copyOf() {
    return new TableauFormula(this);
  }

  private TableauFormula(TableauFormula tf) {
    this.w = tf.w.copyOf();
    this.mf = tf.mf.copyOf();
    this.respectedIn = tf.respectedIn.copyOf();
  }
}
