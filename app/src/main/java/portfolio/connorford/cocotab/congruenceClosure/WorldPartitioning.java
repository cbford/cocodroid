package portfolio.connorford.cocotab.congruenceClosure;

import java.util.ArrayList;

import portfolio.connorford.cocotab.logic.World;

/**
 * Represents the collections of worlds that can be rewritten as eachother.
 * This is a forest of WorldPartitions
 */
@SuppressWarnings("serial")
public class WorldPartitioning extends ArrayList<WorldPartition> {

  /**
   * @return the tree of worlds with rewrites related to w, or a fresh tree
   */
  public WorldPartition getPartition(World w) {
    //System.out.println("find partition of " + w + " in " + this);

    //if in a partition already, return the partition
    for(WorldPartition p : this)
      if(p.contains(w))
        return p;

    //otherwise create a new partition and return that
    WorldPartition wp = new WorldPartition();
    wp.add(w);
    this.add(wp);
    return wp;
  }

  /**
   * @return true iff the world is in some partition
   */
  public boolean contains(World w) {
    boolean inPartitioning = false;

    for(WorldPartition p: this)
      if(p.contains(w))
        inPartitioning = true;

    return inPartitioning;
  }

  /**
   * @return the least term in the world's partition
   */
  public World getInENormalForm(World w) {
    return getPartition(w).getENormal();
  }

  public void addWorldsToPartition(World w1, World w2) {
    //System.out.printf("add to partition: %s and %s\n", w1, w2);

    //determine the two partition the worlds sit in
    //may be that they are in partitions of their own
    WorldPartition p1 = getPartition(w1),
                   p2 = getPartition(w2);

    //if they already belong to the same partition we are done
    if(p1.equals(p2))
      return;
    //merge their partitions
    else {
      for(World w : p1)
        p2.add(w);
      this.remove(p1);
    }

  }

  /**
   * Deep copy for branch splitting
   */
  public WorldPartitioning copy() {
    WorldPartitioning wpg = new WorldPartitioning();
    for(WorldPartition wp : this)
      wpg.add(wp.copy());
    return wpg;
  }
}
