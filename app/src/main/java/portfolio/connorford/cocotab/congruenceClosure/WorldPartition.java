package portfolio.connorford.cocotab.congruenceClosure;

import java.util.TreeSet;

import portfolio.connorford.cocotab.logic.World;

/**
 * Represents a collection of worlds connected by rewrite rules.
 * Ordered by the term ordering.
 */
@SuppressWarnings("serial")
public class WorldPartition extends TreeSet<World>{

  /**
   * Retrieve the world all worlds in the partition as rewritten to.
   *
   * @return the smallest world
   */
  public World getENormal() {
   // System.out.println(this);
   // for(World w : this)
   //   System.out.print(w.hashCode() + " ");
   // System.out.println();
    return first();
  }

  /**
   * Deep copy for branch splitting
   */
  public WorldPartition copy() {
    WorldPartition wp = new WorldPartition();
    for(World w : this)
      wp.add(w.copyOf());
    return wp;
  }
}
