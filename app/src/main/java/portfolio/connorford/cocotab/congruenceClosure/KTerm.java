package portfolio.connorford.cocotab.congruenceClosure;

import portfolio.connorford.cocotab.logic.World;

/**
 * Represents a fresh world term.
 * Used by term rewriting in the congruence closure rules
 */
public class KTerm extends World {
  //class vars that can be used to put bounds on the tableau derivation
  private static int termCount = 0,
                     maxTermCount = 1000;
  //a term is identified uniquely by an index
  private int termIndex;

  /**
   * Reset our indexing.
   * Can be used when multiple tableau are being tested
   */
  public static void resetTermCount()
  {
    termCount = 0;
  }
 
 /**
  * Class constructor.
  * New instance with increased index
  */ 
  public KTerm() {
    super("c" + termCount);
    termIndex = termCount++;
  }

  /**
   * Duplicate term for a new split branch
   */
  public KTerm copyOf() {
    return new KTerm(this);
  }
  
  /**
   * Constructor for duplication
   *
   * @param k  the term to duplicate
   */
  private KTerm(KTerm k) {
    super(k);
    this.termIndex = k.termIndex;
  }

  @Override
  /**
   * @return true iff o is a KTerm with the same term index
   */
  public boolean equals(Object o) {
    if(o instanceof KTerm) {
      //System.out.println("compare " + termIndex + " to " + ((KTerm)o).termIndex);
      return (termIndex == (((KTerm) o).termIndex));
    }
    else
      return false;
  }

  @Override
  /**
   * Get the unique identifier for the KTerm.
   *
   * @return the index, unique with respect to the branch
   */
  public int hashCode() {
    return termIndex;
  }

  @Override
  public int compareTo(World w) {
    //KTerms are always smaller than other world types
    if(!(w instanceof KTerm))
    {
      return -1;
    }
    //ci > cj iff i > j
    else
    {
      KTerm k = (KTerm) w;
      return this.termIndex - k.termIndex;
    }
  }
  
  /**
   * Get the number of newly instantiated KTerms.
   * Used for bounding a tableau
   */
  public static int termCount() {
    return termCount;
  }

  /**
   * Get the total number of allowed new KTerms.
   * Used for bounding a tableau
   */
  public static int getMaxTermCount() {
    return maxTermCount;
  }

  /**
   * Reset the number of instantiated KTerms.
   * Used for bounding a tableau
   */
  public static void resetCount() {
    termCount = 0;
  }
}
