package portfolio.connorford.cocotab.congruenceClosure;

import portfolio.connorford.cocotab.logic.World;
import portfolio.connorford.cocotab.tableau.Deduction;

/**
 * Represents the process of rewriting a term in a deduction
 */
public class RewriteAction extends Deduction {
  Deduction ded;
  World from;
  KTerm to;

  /**
   * Construct a duplicate for a split branch
   *
   * @param ra  the action to duplicate
   */
  private RewriteAction(RewriteAction ra) {
    this.ded = ra.ded.copyOf();
    this.from = ra.from.copyOf();
    this.to = ra.to.copyOf();
  }

  /**
   * Get a duplicate of this action.
   * For splitting branches.
   */
  public RewriteAction copyOf() {
    return new RewriteAction(this);
  }
  
  /**
   * Class constructor.
   * For a fresh rewrite action
   */
  public RewriteAction(Deduction d, World f, KTerm t) {
    ded = d;
    from = f; 
    to = t;
  }
  
  /**
   * @return the deduction in which we want to rewrite a subterm
   */
  public Deduction getDeductionToRewrite() {
    return ded;
  }

  /**
   * @return the term we want to replace in the deduction
   */
  public World getRewriteFrom() {
    return from;
  }

  /**
   * @return the term we want to occur after the rewrite
   */
  public World getRewriteTo() {
    return to;
  }
  
  /**
   * Perform the term rewrite in the deduction
   *
   * @param reason  why we are rewriting in the deduction, a tableau rule
   * @return true iff we successfully rewrote the from subterm with the to
   */
  public boolean doRewrite(String reason) {
    return ded.rewriteWorld(from, to, reason);
  }
  
  @Override
  // Rewrite actions themselves cannot be rewritten by tableaux rules
  public boolean rewriteWorld(World from, World to, String reason) {
    return false;
  }
  
  public String toString() {
    return "in " + ded + ", rewrite " + from + " as " + to;
  }
}
