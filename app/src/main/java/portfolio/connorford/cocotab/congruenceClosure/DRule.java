package portfolio.connorford.cocotab.congruenceClosure;

import portfolio.connorford.cocotab.logic.World;

/**
 * Represents a RewriteRule from a non KTerm to a KTerm
 */
public class DRule extends RewriteRule{
  /**
   * Duplicate rule for a new split branch
   */
  public DRule copyOf() {
    return new DRule(this);
  }

  /**
   * Constructor for a duplicate
   *
   * @param dr  the rule to duplicate
   */
  private DRule(DRule dr) {
    this.from = dr.from;
    this.to = dr.to;
  }

  /**
   * Constructor for a fresh DRule
   */
  public DRule(World world, KTerm t) {
    from = world;
    to = t;
  }

  @Override
  /**
   * Attempt to rewrite the LHS term in the rule.
   * We may rewrite from in a d rule by the collapse congruence closure rule.
   *
   * @return true iff a new rewrite is applied to the from world
   */
  public boolean rewriteWorld(World writeFrom, World writeTo, String reason) {
    if(!(from instanceof WitnessWorld))
      return false;

    WitnessWorld fromAsWit = (WitnessWorld) from;

    if(fromAsWit.containsSubterm(writeFrom))
    {
      return fromAsWit.rewriteWorld(writeFrom, writeTo, reason);
    }

    return false;
  }
}
