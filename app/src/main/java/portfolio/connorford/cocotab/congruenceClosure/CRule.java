package portfolio.connorford.cocotab.congruenceClosure;

import portfolio.connorford.cocotab.logic.World;

/**
 * Represents a rewrite rule from one KTerm to Another
 */
public class CRule extends RewriteRule{
  /**
   * Duplicate rule for a new split branch
   */
  public CRule copyOf() {
    return new CRule(this);
  }

  /**
   * Constructor for a duplicate
   *
   * @param cr  the rule to duplicate
   */
  private CRule(CRule cr) {
    this.from = cr.from;
    this.to = cr.to;
  }

  /**
   * Constructor for a fresh CRule
   */
  public CRule(KTerm f, KTerm t) {
    from = f;
    to = t;
  }

  @Override
  //we don't rewrite worlds in c rules
  public boolean rewriteWorld(World from, World to, String Reason) {
    return false;
  }
}
