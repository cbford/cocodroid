package portfolio.connorford.cocotab.congruenceClosure;

import portfolio.connorford.cocotab.logic.World;
import portfolio.connorford.cocotab.tableau.Deduction;

/**
 * Abstract representation of a CRule or a DRule
 */
public abstract class RewriteRule extends Deduction {
  //a rewrite is from one world to another
  protected World from;
  protected KTerm to;
  
  public World getFirst() {
    return from;
  }
  
  public KTerm getSecond() {
    return to;
  }
  
  public int hashCode() {
    int t1 = from.hashCode(),
        t2 = to.hashCode();
    
    return ((t1+t2)*(t1+t2+1)/2) + t1;
  }
  
  //char may not be supported
  public String toString() {
    return from + "→" + to;
  }
  
  @Override
  public boolean equals(Object o) {
    if(o instanceof CRule && this instanceof CRule) {
      return hashCode() == ((CRule)o).hashCode();
    }
    else if(o instanceof DRule && this instanceof DRule) {
      return hashCode() == ((DRule)o).hashCode();
    }
    else
      return false;
  }
}
