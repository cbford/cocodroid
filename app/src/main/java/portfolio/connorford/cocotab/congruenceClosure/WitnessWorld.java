package portfolio.connorford.cocotab.congruenceClosure;

import portfolio.connorford.cocotab.logic.World;
import portfolio.connorford.cocotab.tableau.DeductionSet;

/**
 * Represents a World created in witness of a formula in another world.
 * Created by application of the diamond(not box)
 * or distinct predecessor(1 or 3) tableaux rules
 */
public class WitnessWorld extends World {
  private static int termCount = 0,
                     maxTermCount = 1000;
  private int termIndex = 0;
  //a witness world has a world to witness and a witness function type (f or g)
  //a witness world can witness a witness world, in which case we consider it
  //as a linked list with a non-witness world as the last element
  private World witnessOf;
  private String witnessLabel;
  
  /**
   * Class constructor for a fresh witness term
   */
  public WitnessWorld(String witnessLbl, World worldToWitness) {
    super(witnessLbl + "(" + worldToWitness + ")");
    termIndex = termCount++;
    eNormal = true;
    witnessOf = worldToWitness;
    witnessLabel = witnessLbl;
  }

  /**
   * Get duplicate(deep copy) for branch splitting
   */
  public WitnessWorld copyOf()
  {
    return new WitnessWorld(this);
  }

  /**
   * Deep copy constructor
   *
   * @param w  The world to duplicate
   */
  public WitnessWorld(WitnessWorld w)
  {
    super(w.witnessLabel + "(" + w.witnessOf + ")");
    this.termIndex = w.termIndex;
    this.eNormal = w.eNormal;
    this.witnessOf = w.witnessOf;
    this.witnessLabel = w.witnessLabel;
  }

  public String toString() {
    return witnessLabel + "(" + witnessOf + ")";
  }
  
  @Override
  public boolean equals(Object o) {
    if(o instanceof WitnessWorld) {
      WitnessWorld w = (WitnessWorld)o;
      return (witnessOf.equals(w.witnessOf) && witnessLabel.equals(w.witnessLabel));
    }
    else
      return false;
  }
  
  @Override
  public int hashCode() {
    //witness terms are greater than kterms
    return termIndex%1000 + KTerm.getMaxTermCount();
  }

  @Override
  public int compareTo(World w) {
    //System.out.println(w + " v " + this + " " + w.equals(this));
    if(w instanceof KTerm) //KTerms are always smallest
     return 1;
    else if(w instanceof WitnessWorld)
     return (w.equals(this)) ? 0 : this.hashCode() - ((WitnessWorld)w).hashCode();
    else //it is a labelled world
      return -1;
  }
  
  //for these 3 methods refer to counterparts in the KTerm class
  public static int termCount() {
    return termCount;
  }
  
  public static int getMaxTermCount() {
    return maxTermCount;
  }
  
  public static void resetCount() {
    termCount = 0;
  }
  
  /**
   * @return the world which this world is witnessing a formula from
   */
  public World witnessOf() {
    return witnessOf;
  }

  @Override
  /**
   * Get smallest subterm that is not a KTerm.
   * Used by congruence closure rules
   *
   * @return the smallest such subterm, which may be the term itelf
   */
  public World getSmallestSubterm() {
    if(witnessOf instanceof KTerm)
      return this;
    else if(witnessOf instanceof WitnessWorld && !(((WitnessWorld) witnessOf).witnessOf instanceof KTerm))
      return ((WitnessWorld)witnessOf).getSmallestSubterm();
    else
      return witnessOf;
  }
  
  /**
   * @return f or g, the witness type, determined by the rule applied
   */
  public String getWitnessLabel() {
    return witnessLabel;
  }

  /**
   * @return a DeductionSet of all subterms, the elements in our linked list
   */
  public DeductionSet<World> getSubterms() {
    DeductionSet<World> ret = new DeductionSet<World>();

    World term = this;

    while(term instanceof WitnessWorld)
    {
      ret.add(((WitnessWorld)term).witnessOf);
      term = ((WitnessWorld)term).witnessOf;
    }

    return ret;
  }

  public boolean containsSubterm(World sub) {
    return getSubterms().contains(sub);
  }


  @Override
  /**
   * Rewrite the from with to if the from is a subterm of this world
   *
   * @return true iff a rewrite is performed
   */
  public boolean rewriteWorld(World from, World to, String reason)
  {
    //System.out.print(this + " has subterms " + getSubterms());
    //for(World w : getSubterms())
    // System.out.printf("\t%s=%s?%s\n" , from, w, w.equals(from));
    
    //attempt to find the from world as a subterm of the witness world
    if(containsSubterm(from))
    {
      if(witnessOf.equals(from))
      {
        //witnessOf = new World(to);
        witnessOf = to;
      }
      else
      {
        witnessOf.rewriteWorld(from, to, reason);
      }
      return true;
    }
    else
    {
      return false;
    }
  }
}
