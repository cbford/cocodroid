package portfolio.connorford.cocotab.congruenceClosure;

import java.util.Iterator;

import portfolio.connorford.cocotab.logic.World;
import portfolio.connorford.cocotab.tableau.Branch;
import portfolio.connorford.cocotab.tableau.Deduction;
import portfolio.connorford.cocotab.tableau.DeductionSet;
import portfolio.connorford.cocotab.tableau.WorldEquality;

/**
 * Congruence Closure.
 * Represents deductions about world equality on a Tableau branch
 */
public class CongruenceClosure
{

  //the branch on which we make deductions
  private Branch currBranch;

  /**
   * Class constructor
   *
   * @param b  The branch on which we want to apply congruence closure rules
   */
  public CongruenceClosure(Branch b)
  {
    currBranch = b;
  }

  /**
   * A congruence closure rule that rewrites terms exhaustively
   */
  private void simplify()
  {
    DeductionSet<RewriteRule> rewriteRules = currBranch.getRewriteRules();
    boolean simplifying = true;
    while(simplifying)
    {
      //get rules
      Iterator<RewriteRule> simRules = rewriteRules.iterator();
      //try to simplify on them
      RewriteRule simRule;
      boolean simplified = false;
      while(!simplified && simRules.hasNext())
      {
        simRule = simRules.next();
        World writeFrom = simRule.getFirst();
        KTerm writeTo = simRule.getSecond();
        for(Deduction nTerm : currBranch.getNTerms())
        {
          //System.out.println("simplify " + nTerm + " with? " + simRule);
          if(!simplified)
          {
            simplified = currBranch.addDeduction(new RewriteAction(nTerm, writeFrom, writeTo), "simplification");
          }
          //System.out.println("\t"+simplifying + " " + simRules.hasNext());
        }
      }
      if(!simplified)
        simplifying = false;
    }
  }

  /**
   * Replace a term with a KTerm in an equality
   *
   * @param eq  the equality in which to replace a term
   * @param w  the term we replace
   * @return true iff we replace with a new deduction in the branch
   */
  private boolean extension(WorldEquality eq, World w)
  {
    //System.out.println("extending " + w +  " on " + eq);
    World extendIn;

    //determine which term in the equality we are rewriting in
    if(eq.getFirst().equals(w))
    {
      extendIn = eq.getFirst();
    }
    else if(eq.getSecond().equals(w))
    {
      extendIn = eq.getSecond();
    }
    else
    {
      return false;
    }

    //rewrite the smallest subterm that is not a KTerm
    //to respect term ordering
    World t = extendIn.getSmallestSubterm();

    //System.out.println("subterm of " + extendIn + " is " + t);

    return extend(eq, t);
  }

  /**
   * Add an extension deduction to the branch
   *
   * @return true iff a new deduction is added
   */
  private boolean extend(Deduction toRewrite, World writeFrom)
  {
    KTerm writeTo = new KTerm();
    RewriteAction extensionSubstitute = new RewriteAction(toRewrite, writeFrom, writeTo);
    DRule extensionRule = new DRule(writeFrom, writeTo);

    currBranch.addDeduction(extensionRule, "extension");
    return currBranch.addDeduction(extensionSubstitute, "extension");
  }


  /**
   * Rewrite inside the left hand side of a a DRule with an applicable CRule.
   * Applied exhaustively.
   *
   * @return A newly collapsed DRule or null if no rewrite possible
   */
  private DRule collapse()
  {
    //System.out.printf("collapse on\n\t%s\t%s", currBranch.getWorldEqualities(), currBranch.getRewriteRules());

    DRule rule;
    Iterator<DRule> rules = currBranch.getDRules().iterator();

    //iterate over DRules in which we may rewrite a term
    while(rules.hasNext())
    {
      rule = rules.next();
      if(rule.getFirst() instanceof WitnessWorld)
      {
        WitnessWorld wit = (WitnessWorld) rule.getFirst();

        //collapse on any applicable CRule
        for(CRule cRule : currBranch.getCRules())
        {
          World c = cRule.getFirst();
          KTerm d = cRule.getSecond();
          if(wit.containsSubterm(c))
          {
            RewriteAction rewriteRule = new RewriteAction(rule, c, d);
            currBranch.addDeduction(rewriteRule, "collapse");
            return rule;
          }
        }
      }
    }
    return null;
  }

  /**
   * Reduce 2 RewriteRules to one.
   * The right hand terms are compared, and the rule with the larger
   * under term ordering is removed. We add a new equality for the removed
   * world. Applied exhaustively.
   */
  private void deduce()
  {
    //System.out.printf("deduce on\n\t%s\t%s", currBranch.getWorldEqualities(), currBranch.getRewriteRules());
    boolean deducing = true;
    while(deducing)
    {
      deducing = false;
      Iterator<RewriteRule> rules1 = currBranch.getRewriteRules().iterator(),
                      rules2 = currBranch.getRewriteRules().iterator();
      while(rules1.hasNext() && !deducing)
      {
        RewriteRule r1 = rules1.next();
        rules2 = currBranch.getRewriteRules().iterator();
        while(rules2.hasNext() && !deducing)
        {
          RewriteRule r2 = rules2.next();
          //System.out.printf("1: %s 2: %s\n", r1, r2);
          if(!r1.equals(r2) && r1.getFirst().equals(r2.getFirst()))
          {
            deducing = true;
            //determine smaller of seconds
            KTerm r1Second = r1.getSecond(),
                  r2Second = r2.getSecond();

            WorldEquality deducedEq = new WorldEquality(r1Second, r2Second, true);
            currBranch.addDeduction(deducedEq, "deduction");

            if(r1Second.compareTo(r2Second) > 0)
            {
              //r1 second is bigger
              currBranch.addDeduction(r1, "deduction");
            }
            else
            {
              currBranch.addDeduction(r2, "deduction");
            }
          }
        }
      }
    }
  }

  /**
   * Implementation of Shostak's algorithm for congruence closure.
   * Combines the above rules to produce a congruence clousre for the branch
   */
  public void applyShostak()
  {
    WorldEquality eq;

    //algorithm applied while no unoriented equations remain
    //and we have no contradiction
    while(!(currBranch.getWorldEqualities().isEmpty()) && !currBranch.isClosed())
    {
      //System.out.printf("\t%s\t%s\t%s", currBranch.getWorlds(), currBranch.getWorldEqualities(), currBranch.getRewriteRules());
      
      //we terminate and simplify if we can no longer orient equations
      if(currBranch.getPositiveWorldEqualities().isEmpty())
      {
        simplify();
        break;
      }

      //reduce eq := s=t to constants
      eq = currBranch.getPositiveWorldEqualities().peek();
      World s = eq.getFirst(),
            t = eq.getSecond();

      boolean extending = true;

      //first reduce s to a KTerm
      while(!((s = eq.getFirst()) instanceof KTerm) && !currBranch.isClosed())
      {
        //sim*
        simplify();
        //ext?
        extending = extension(eq, s);
      }
      //then t
      extending = true;
      while(!((t = eq.getSecond()) instanceof KTerm) && !currBranch.isClosed())
      {
        //sim*
        simplify();
        //ext?
        extending = extension(eq, t);
      }

      //the reduction may produce a contradiction
      if(currBranch.isClosed())
        continue;

      //now label s,t as KTerms c,d with c>=d
      //swapping if c<d
      KTerm c, d;
      if(eq.getFirst().compareTo(eq.getSecond()) > 0)
      {
        c = (KTerm) eq.getFirst();
        d = (KTerm) eq.getSecond();
      }
      else
      {
        c = (KTerm) eq.getSecond();
        d = (KTerm) eq.getFirst();
      }

      //if c=d we apply deletion
      if(c.equals(d))
      {
        currBranch.addDeduction(eq, "deletion");
      }
      //otherwise we apply ori then collpase and deduction
      else if(eq.getEquality())      {
        CRule orientationRule = new CRule(c, d);
        currBranch.addDeduction(orientationRule, "orientation");
        currBranch.getWorldEqualities().remove(eq);

        //now (col .ded*)*
        CRule collapseRule = orientationRule;
        DRule collapseResult;
        boolean collapsing = true;
        while(collapsing)
        {
           collapseResult = collapse();
           //System.out.println(collapseResult);
           deduce();
           collapsing = (collapseResult != null);
        }
      }
      else
      {
        //cant del or ori on an inequality, move it to the back
        currBranch.getWorldEqualities().addLast(currBranch.getWorldEqualities().pop());
      }
    }
    simplify();
  }
}
