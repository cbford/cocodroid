# README #

An android port of Cocotab, a semantic tableaux theorem prover for modal logic implementing
congruence closure. It is provided as an AndroidStudio project.

The report for the original third year project can be found in [cocotabReport.pdf](./cocotabReport.pdf).

The project depends on the ANTLR4 library provided as a jar in libs.
This version of ANTLR is a fork of 4.5.1 modified to run on android
by removing the classes containing references to Swing packages.

Before cloning this repository you must read [COPYING.md](./COPYING.md) and agree to the terms within.